<?php
require __DIR__ . '/../test-tools.php';

$b = new EventBase();
$b->priorityInit(10);

$ed = new Event($b, SIGKILL, Event::SIGNAL, function() { });
$ed->free();
@t("free", false, $ed->add(1));

$e = new Event($b, -1, Event::TIMEOUT, function($arg) {
    t1("timeout callback", ($arg == 'data'));
    //$b->exitLoop();
  }, 'data');
t1("add", $e->add(0));
t1("setPriority true", $e->setPriority(9));
t("setPriority false", false, $e->setPriority(90));
t1("pending", $e->pending(Event::ALL_TYPES));
t1("del", $e->del());
t("pending", false, $e->pending(Event::ALL_TYPES));
t1("add", $e->add(.5));
t1("pending", $e->pending(Event::ALL_TYPES));
t1("dispatch", $b->dispatch());
t1("loop", $b->loop(EventBase::LOOP_ONCE));
t1("getSupportedMethods", (count(Event::getSupportedMethods() >= 2)));

t1("set", $e->set($b, -1, Event::TIMEOUT, function($fd, $what, $arg) use ($b) {
  t1("set(timeout callback)", ($arg == 'data2' && $what & Event::TIMEOUT && $fd == -1));
  //$b->exitLoop();
}, 'data2'));
t1("add", $e->add(.3));
t1("dispatch", $b->dispatch());

t1("setTimer", $e->setTimer($b, function ($arg) {
  t1("setTimer(timeout callback)", ($arg == 'data3'));
  //$b->exitLoop();
}, 'data3'));


$sig = Event::signal($b, SIGUSR2, function ($signum, $arg) use ($b) {
  t1("signal(callback)", ($signum == SIGUSR2 && $arg == 'xxx'));
  //$b->exitLoop();
}, 'xxx');
t1("signal", $sig->add(0.2));
//$b->loop(EventBase::LOOP_NONBLOCK);
posix_kill(getmypid(), SIGUSR2);
$b->dispatch();

$t = Event::timer($b, function($arg) {
  t1('timer(callback)', ($arg == 'yyy'));
}, 'yyy');
t1("timer", $t->add(0.3));
$b->dispatch();
