<?php
require __DIR__ . '/../test-tools.php';

$cfg = new EventConfig();
$cfg->requireFeatures(EventConfig::FEATURE_FDS);

$b = new EventBase();
$b = new EventBase($cfg);
t1("getFeatures", ($b->getFeatures() == EventConfig::FEATURE_FDS));
//t("getMethod", "poll", $b->getMethod());
t1("getTimeOfDayCached", (abs($b->getTimeOfDayCached() - time()) < 3));
t("gotExit", false, $b->gotExit());
t1("priorityInit", $b->priorityInit(10));
t("gotStop false", false, $b->gotStop());
$b->stop();
t("gotStop true", true, $b->gotStop());
