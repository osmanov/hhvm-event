<?php

require __DIR__ . '/../test-tools.php';

$b = new EventBase();

function readcb($base) {
  echo __FUNCTION__, " start\n";
  global $bev;

  for ($bytes = 0, $i = 0, $in = $bev->input;
  !empty($buf = $in->read(1024)) > 0;
  ++$i)
  {
    $bytes += strlen($buf);
  }
  t1('input->read()', ($bytes > $i));
  echo __FUNCTION__, " end\n";
}

function eventcb($events, $base) {
  echo __FUNCTION__, " start\n";
  global $bev;

  if ($events & EventBufferEvent::CONNECTED) {
    t1('EventBufferEvent::CONNECTED', TRUE);
  } elseif ($events & (EventBufferEvent::ERROR | EventBufferEvent::EOF)) {
    if ($events & EventBufferEvent::ERROR) {
      fprintf(STDERR, "DNS error: ", $bev->getDnsErrorString() . PHP_EOL);
      t1('DNS error', FALSE);
    } else {
      t1('EventBufferEvent::ERROR', TRUE);
    }

    t1('EventBase::exitLoop', $base->exitLoop());
  }
  echo __FUNCTION__, " end\n";
}

$dns_base = new EventDnsBase($b, TRUE); // We'll use async DNS resolving
if (!$dns_base) {
  exit("Failed to init DNS Base\n");
}

$bev = new EventBufferEvent($b, /* use internal socket */ NULL,
  EventBufferEvent::OPT_CLOSE_ON_FREE | EventBufferEvent::OPT_DEFER_CALLBACKS,
  "readcb", /* writecb */ NULL, "eventcb", $b
);
t1('EventBufferEvent::__construct', $bev);
//$bev->setCallbacks("readcb", /* writecb */ NULL, "eventcb", $b);
t1('enable', $bev->enable(Event::READ | Event::WRITE));

$host = 'google.com';
$output = $bev->output;
$output2 = $bev->getOutput();
//$input = $bev->getInput();
t1('getOutput', ($output2 instanceof EventBuffer));
//t1('getInput', ($input instanceof EventBuffer));
t1('output->add()', $output->add(
  "GET / HTTP/1.0\r\n".
  "Host: $host\r\n".
  "Connection: Close\r\n\r\n"
));

t1("connectHost $host", $bev->connectHost($dns_base, $host, 80, EventUtil::AF_UNSPEC));
t1('dispatch', $b->dispatch());
$bev->free(); // no segfault?
$bev = NULL; // still no segfault?
$in = null;
