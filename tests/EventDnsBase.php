<?php
require __DIR__ . '/../test-tools.php';

$b = new EventBase();

$dns1 = new EventDnsBase($b, true);
$dns2 = new EventDnsBase($b, false);
$nns1 = $dns1->countNameservers();
$nns2 = $dns2->countNameservers();
t1('countNameservers 1', ($nns1 > 0));
t1('countNameservers 2', ($nns2 == 0));
t1('addNameserverIp 1', $dns1->addNameserverIp('8.8.4.4'));
t1('addNameserverIp 2', $dns2->addNameserverIp('8.8.4.4'));
t1('countNameservers 1-2', ($dns1->countNameservers() - $nns1 == 1));
t1('countNameservers 2-2', ($dns2->countNameservers() == 1));

$filename = __DIR__.'/tmp.hosts'.uniqid();
file_put_contents($filename, "tmp.test.host 127.0.0.1");
t1('loadHosts 1', $dns1->loadHosts($filename));
t1('loadHosts 2', $dns2->loadHosts($filename));
unlink($filename);

file_put_contents($filename, "nameserver 8.8.8.8");
t1('parseResolvConf 1', $dns1->parseResolvConf(EventDnsBase::OPTIONS_ALL, $filename));
t1('parseResolvConf 2', $dns2->parseResolvConf(EventDnsBase::OPTIONS_ALL, $filename));
unlink($filename);

t1('setOption 1', $dns1->setOption('attempts', 20));
t1('setOption 2', $dns2->setOption('attempts', 20));

$dns1->setSearchNdots(3);

$dns1->addSearch('google.com');
$dns2->addSearch('google.com');
$dns1->clearSearch();
$dns2->clearSearch();
