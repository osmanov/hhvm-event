<?php
require __DIR__ . '/../test-tools.php';

// Count total occurances of 'str' in 'buf'
function _count_buffer_instances($buf, $str) {
  $total = 0;
  $p = 0;

  while (1) {
    $p = $buf->search($str, $p);
    if ($p === FALSE) {
      break;
    }
    ++$total;
    ++$p;
  }

  return $total;
}

$b = new EventBase();

$buf = new EventBuffer();
t1('add', $buf->add('data'));
$buf2 = new EventBuffer();
t1('add', $buf->addBuffer($buf2));
t('getLength', strlen('data'), $buf->getLength());
t1('getContiguousSpace', ($buf->getContiguousSpace() > 0));
t('appendFrom', $buf2->getLength(), $buf->appendFrom($buf2, $buf2->getLength()));
t1('getLength', ($buf2->getLength() == 0));

$buf = new EventBuffer();
$s = '123456789';
$buf->add($s);
$len = strlen($s);
t('copyout', $len, $buf->copyout($data, $len));
t1('copyout (2)', ($data == $s));

t1('drain', ($buf->drain($len - 1) == $s[$len - 1]));
t('drain-copyout', 1, $buf->copyout($data, 1000));
t1('drain-data', ($data == $s[$len - 1]));
t1('drain', ($buf->drain(100) == 1));
$buf->copyout($data, 100);
t1('drain-data', ($data == NULL));


$buf = new EventBuffer();
$s = '123456789';
$buf->add($s);
t1("pullup(-1)", ($s == $buf->pullup(-1)));
t1("pullup(N)", $buf->pullup(3) == substr($s, 0, 3));
t1("read(-1)", ($buf->read(-1) == NULL));
t1("read(N)", ($buf->read(5) == '12345'));
t1("read(555)", ($buf->read(555) == substr($s, 5)));
t1("read(555) = FALSE", ($buf->read(555) == FALSE));
// XXX t1("readFrom", ($howmuch == $buf->readFrom($fd, $howmuch)));

$buf = new EventBuffer();
$lines = ['line 1', 'line 2', 'line 3'];
$buf->add(implode("\n", $lines) . "\n");
foreach ($lines as $i => $l) {
  t1("readLine $i", ($l == $buf->readLine(EventBuffer::EOL_ANY)));
}
t1("readLine FALSE", (FALSE == $buf->readLine(EventBuffer::EOL_ANY)));

///////////////////////////////////////////////////////
// EventBuffer::search
// 1 12 123 1234 .. 123..9
$i = 1;
$s = "";
$a = "";
while ($i < 10) {
  $s .= $i;
  $a .= $s ." ";
  ++$i;
}
$buf = new EventBuffer();
$buf->add($a);
$res = true;
while (--$i > 0) {
  /*
    9 - 1
    8 - 2
    7 - 3
    6 - 4
    5 - 5
    4 - 6
    3 - 7
    2 - 8
    1 - 9
   */
  //echo $i, " - ", _count_buffer_instances($buf, $i), "\n";
  t1("search $i", ($i + _count_buffer_instances($buf, $i) == 10));
}

///////////////////////////////////////////////////////
// EventBuffer::searchEol
$buf = new EventBuffer();
$buf->add("123\n567\n89");
t1("searchEol default", ($buf->searchEol() == 3));
t1("searchEol start", ($buf->searchEol(4) == 7));
t1("searchEol FALSE", ($buf->searchEol(8) === FALSE));


///////////////////////////////////////////////////////
// EventBuffer::substr
$s = "";
$buf = new EventBuffer();
for ($i = 0; $i < 10; ++$i) { $s .= $i; }
$buf->add($s);

$success = TRUE;
for ($i = 0; $i < 10; ++$i) {
  for ($j = 1; $j < 10; ++$j) {
    if ($buf->substr($i, $j) != substr($s, $i, $j)) {
      $success = FALSE;
      break;
    }
  }
}
t1("substr", $success);

t1("freeze", $buf->freeze(true));
t1("unfreeze", $buf->unfreeze(true));
t1("freeze 2", $buf->freeze(0));
t1("unfreeze 2", $buf->unfreeze(false));
