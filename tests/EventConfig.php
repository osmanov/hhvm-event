<?php
require __DIR__ . '/../test-tools.php';

$cfg = new EventConfig();
$b = new EventBase($cfg);
$b = new EventBase();
$cfg = new EventConfig();
$b = new EventBase($cfg);

t1(1, ($cfg->avoidMethod('select') && $cfg->avoidMethod('epoll')));
t(2, '1,2,4', implode(',', [EventConfig::FEATURE_ET, EventConfig::FEATURE_O1, EventConfig::FEATURE_FDS]));
t1(3, $cfg->requireFeatures(EventConfig::FEATURE_FDS));
