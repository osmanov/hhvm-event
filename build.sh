#!/bin/bash -

if [ -z "$HPHP_HOME" ]; then
	echo 1>&2 "\$HPHP_HOME environment variable must be set!"
	exit 1
fi

if [ ! -x "$HPHP_HOME"/hphp/tools/hphpize/hphpize ]; then
	echo 1>&2 "hphpize is not available. Run the following then try again
		cd $HPHP_HOME && cmake . && make"
	exit 1
fi

"$HPHP_HOME"/hphp/tools/hphpize/hphpize
cmake -D HPHP_EVENT_DEBUG=ON .
make
