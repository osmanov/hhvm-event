# -D HPHP_DISABLE_EVENT_EXTRA:STRING=ON
option(HPHP_DISABLE_EVENT_EXTRA "Disable Event protocol-specific functionality support including HTTP, DNS, and RPC" OFF)
# -D HPHP_DISABLE_EVENT_OPENSSL:STRING=OFF
option(HPHP_DISABLE_EVENT_OPENSSL "Disable Event OpenSSL support" OFF)
# -D HPHP_DISABLE_EVENT_PTHREADS:STRING=ON
option(HPHP_DISABLE_EVENT_PTHREADS "Disable pthreads library and enable thread safety support in Event" ON)
# -D HPHP_EVENT_DEBUG:STRING=OFF
option(HPHP_EVENT_DEBUG "Enable debugging support in Event" OFF)

include_directories("${CMAKE_CURRENT_SOURCE_DIR}")
set(CMAKE_MODULE_PATH "${CMAKE_CURRENT_SOURCE_DIR}/CMake" ${CMAKE_MODULE_PATH})
set (HPHP_EVENT_SRC
  ext_event.cpp
  src/EventRequestData.cpp
  api/EventConfig.cpp
  api/EventBase.cpp
  api/Event.cpp
  api/EventBuffer.cpp
  api/EventBufferEvent.cpp
  api/EventUtil.cpp)

find_package(LibEvent REQUIRED)

if (NOT LIBEVENT_EXTRA_LIB)
  set (HPHP_DISABLE_EVENT_EXTRA ON)
  add_definitions(-DHPHP_DISABLE_EVENT_EXTRA)
else (NOT LIBEVENT_EXTRA_LIB)
  set(HPHP_EVENT_SRC ${HPHP_EVENT_SRC} api/EventDnsBase.cpp)
endif (NOT LIBEVENT_EXTRA_LIB)

if (NOT LIBEVENT_OPENSSL_LIB)
  set(HPHP_DISABLE_EVENT_OPENSSL ON)
  add_definitions(-DHPHP_DISABLE_EVENT_OPENSSL)
else (NOT LIBEVENT_OPENSSL_LIB)
  set(HPHP_EVENT_SRC ${HPHP_EVENT_SRC} api/EventSslContext.cpp)
endif (NOT LIBEVENT_OPENSSL_LIB)

if (NOT LIBEVENT_PTHREADS_LIB)
  set(HPHP_DISABLE_EVENT_PTHREADS ON)
  add_definitions(-DHPHP_DISABLE_EVENT_PTHREADS)
endif (NOT LIBEVENT_PTHREADS_LIB)

if (HPHP_EVENT_DEBUG)
  MESSAGE (STATUS "Build type: Debug")
  set(CMAKE_BUILD_TYPE "Debug")
  add_definitions(-DHPHP_EVENT_DEBUG)
endif (HPHP_EVENT_DEBUG)

set(CMAKE_REQUIRED_INCLUDES "${LIBEVENT_INCLUDE_DIR}")
set(CMAKE_REQUIRED_LIBRARIES "${LIBEVENT_CORE_LIB}")
file(READ ${CMAKE_CURRENT_SOURCE_DIR}/CMake/CheckLibeventVersion.c VERSION_CHECK_SOURCE)
check_c_source_compiles("${VERSION_CHECK_SOURCE}" HAVE_LIBEVENT_202)

if (HAVE_LIBEVENT_202)
  message(STATUS "Found libevent 2.0.2-alpha+")
else (HAVE_LIBEVENT_202)
  message(FATAL_ERROR "This extension requires at least libevent version 2.0.2-alpha")
endif (HAVE_LIBEVENT_202)

if (NOT HPHP_DISABLE_EVENT_OPENSSL)
  find_package(OpenSSL)
endif (NOT HPHP_DISABLE_EVENT_OPENSSL)
if (NOT OPENSSL_FOUND)
  message(WARNING "Disabling Event OpenSSL support")
  set(HPHP_DISABLE_EVENT_OPENSSL ON)
  add_definitions(-DHPHP_DISABLE_EVENT_OPENSSL)
  set(OPENSSL_INCLUDE_DIR "")
  set(OPENSSL_CRYPTO_LIBRARY "")
  set(OPENSSL_LIBRARIES "")
  set(LIBEVENT_OPENSSL_LIB "")
endif (NOT OPENSSL_FOUND)

include_directories(${LIBEVENT_INCLUDE_DIR})

HHVM_EXTENSION(event ${HPHP_EVENT_SRC})
HHVM_SYSTEMLIB(event ext_event.php)
HHVM_LINK_LIBRARIES(event
  ${LIBEVENT_CORE_LIB}
  ${LIBEVENT_EXTRA_LIB}
  ${LIBEVENT_OPENSSL_LIB}
  ${LIBEVENT_PTHREADS_LIB})

# vim: et ts=2 sts=2 sw=2
