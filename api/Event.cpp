/*
   +----------------------------------------------------------------------+
   | HipHop for PHP                                                       |
   +----------------------------------------------------------------------+
   | Copyright (c) 2010-2014 Facebook, Inc. (http://www.facebook.com)     |
   | Copyright (c) 1997-2014 The PHP Group                                |
   | Author: Ruslan Osmanov <osmanov@php.net>                             |
   +----------------------------------------------------------------------+
   | This source file is subject to version 3.01 of the PHP license,      |
   | that is bundled with this package in the file LICENSE, and is        |
   | available through the world-wide-web at the following url:           |
   | http://www.php.net/license/3_01.txt                                  |
   | If you did not receive a copy of the PHP license and are unable to   |
   | obtain it through the world-wide-web, please send a note to          |
   | license@php.net so we can mail you a copy immediately.               |
   +----------------------------------------------------------------------+
*/

#include "api/Event.h"

namespace HPHP {

/////////////////////////////////////////////////////////////////////////
// Internal

#define SET_FD(__what, __fd, __fdvar, __ret)                       \
  do {                                                             \
    if ((__what) & EV_SIGNAL) {                                    \
      (__fd) = static_cast<evutil_socket_t>((__fdvar).toInt64());  \
      if (!check_signum((__fd))) {                                 \
        raise_warning("invalid signal passed");                    \
        return __ret;                                              \
      }                                                            \
    } else if ((__what) & EV_TIMEOUT) {                            \
      fd = -1;                                                     \
    } else {                                                       \
      (__fd) = Event::varToFd((__fdvar));                          \
      if ((__fd) < 0) {                                            \
        raise_warning("invalid socket or file descriptor passed"); \
        return __ret;                                              \
      }                                                            \
    }                                                              \
  } while (0)

#define CHECK_PENDING(e, __ret)                    \
  do {                                             \
    if ((e)->isPending()) {                        \
      raise_warning("can't modify pending event"); \
      return __ret;                                \
    }                                              \
  } while (0)



ALWAYS_INLINE
static bool check_signum(evutil_socket_t fd) {
  return (fd >= 0 && fd < NSIG);
}

static void event_callback(evutil_socket_t fd, short what, void* arg) {
  assert(arg);
  Event* rc = static_cast<Event*>(arg);
  assert(!rc->isInvalid());
  rc->callback(fd, what);
}

static void timer_callback(evutil_socket_t fd, short what, void* arg) {
  assert(arg);
  Event* rc = static_cast<Event*>(arg);
  assert(!rc->isInvalid());
  rc->callback();
}

static void signal_callback(evutil_socket_t signum, short what, void* arg) {
  assert(arg);
  Event* rc = static_cast<Event*>(arg);
  assert(!rc->isInvalid());
  rc->callback(signum);
}

evutil_socket_t Event::varToFd(const Variant& var) {
  auto type = var.getType();

  switch (type) {
    case KindOfResource:
      try {
        Socket* sock = var.toResource().getTyped<Socket>();
        if (!sock) {
          break;
        }
        if (!sock->valid()) {
          break;
        }

        return static_cast<evutil_socket_t>(sock->fd());
      } catch (const InvalidObjectTypeException& e) {
        raise_warning("%s", e.what());
      }
      break;
    case KindOfInt64:
      return static_cast<evutil_socket_t>(var.toInt64());
    default:
      return -1;
  }

  return -1;
}


Event::Event()
: m_ptr(nullptr), m_cb(null_variant), m_cbArg(null_variant) {}


Event::Event(struct event_base* base, evutil_socket_t fd, short what, const Variant& cb, const Variant& arg)
: m_cb(cb), m_cbArg(arg) {
  assert(base);

  if (what & EV_SIGNAL) {
    m_ptr = ::evsignal_new(base, fd, signal_callback, static_cast<void*>(this));
  } else if (what & EV_TIMEOUT) {
    m_ptr = ::evtimer_new(base, timer_callback, static_cast<void*>(this));
  } else {
    m_ptr = ::event_new(base, fd, what, event_callback, static_cast<void*>(this));
  }

  if (!m_ptr) {
    raise_error("event_new failed");
    return;
  }
}

void Event::free() {
  if (m_ptr) {
    // No need in ::event_del(m_ptr) since
    // ::event_free makes event non-pending internally
    ::event_free(m_ptr);
    m_ptr = nullptr;
  }
}


Event::~Event() {
  free();
}


void Event::sweep() {
  free();
}

Event* Event::get(Object obj) {
  return getInternalResource<Event>(obj, s_Event);
}

Object Event::wrap() {
  return newInstance(s_Event.get());
}

bool Event::isPending() {
  return m_ptr
    ? ::event_pending(m_ptr, EV_READ | EV_WRITE | EV_SIGNAL | EV_TIMEOUT, NULL)
    : false;
}

void Event::callback(evutil_socket_t fd, short what) const {
  if (MemoryManager::sweeping()) return;
  vm_call_user_func(m_cb, make_packed_array(fd, what, m_cbArg));
}

void Event::callback() const {
  if (MemoryManager::sweeping()) return;
  vm_call_user_func(m_cb, make_packed_array(m_cbArg));
}

void Event::callback(evutil_socket_t fd) const {
  if (MemoryManager::sweeping()) return;
  vm_call_user_func(m_cb, make_packed_array(fd, m_cbArg));
}


/////////////////////////////////////////////////////////////////////////
// Event PHP class

static void HHVM_METHOD(Event, __constructor, const Object& baseObj, const Variant& fdvar, int64_t what, const Variant& cb, const Variant& arg/* = null_variant */) {
  evutil_socket_t fd;

  HPHP_EVENT_BASE_GET(base, baseObj, /* void */);
  assert(base);

  SET_FD(what, fd, fdvar, /* void */);

  HPHP_EVENT_CHECK_CALLABLE(cb, /* void */);

  auto rc = NEWOBJ(Event)(base->getPtr(), fd, what, cb, arg);
  this_->o_set(s__rc, Resource(rc), s_Event.get());
}

static bool HHVM_METHOD(Event, add, double timeout = -1.0) {
  HPHP_EVENT_GET(event, this_, false);

  if (timeout == -1.0) {
    return (::event_add(event->getPtr(), NULL) == 0);
  }

  struct timeval tv;
  HPHP_EVENT_TIMEVAL_SET(tv, timeout);

  return (::event_add(event->getPtr(), &tv) == 0);
}

static bool HHVM_METHOD(Event, del) {
  HPHP_EVENT_GET(event, this_, false);
  return (::event_del(event->getPtr()) == 0);
}

static void HHVM_METHOD(Event, free) {
  HPHP_EVENT_GET(event, this_, /* void */);
  event->free();
}

static Array HHVM_STATIC_METHOD(Event, getSupportedMethods) {
  const char **methods = ::event_get_supported_methods();
  Array a;

  for (int i = 0; methods[i] != NULL; ++i) {
    a.add(i, methods[i]);
  }

  return a;
}

static bool HHVM_METHOD(Event, pending, int64_t flags) {
  HPHP_EVENT_GET(event, this_, false);

  return (::event_pending(event->getPtr(), flags, NULL));
}

static bool HHVM_METHOD(Event, set, const Object& baseObj, const Variant& fdvar,
    int64_t what /*= -1*/,
    const Variant& cb /*= null_variant*/,
    const Variant& arg /*= null_variant*/) {
  evutil_socket_t fd;

  SET_FD(what, fd, fdvar, false);

  HPHP_EVENT_CHECK_CALLABLE(cb, false);
  HPHP_EVENT_GET(event, this_, false);

  if (event->isPending()) {
    raise_warning("can't modify pending event");
    return false;
  }

  HPHP_EVENT_BASE_GET(base, baseObj, false);
  // XXX check if a signum bound to different event bases

  event->set(cb, arg);

  struct event_base* basePtr = base->getPtr();
  ::event_get_assignment(event->getPtr(), &basePtr,
      (fd >= 0 ? NULL : &fd),
      reinterpret_cast<short*>(what == -1 ? &what : NULL),
      NULL /* ignore old callback */ ,
      NULL /* ignore old callback argument */);

  return (::event_assign(event->getPtr(), base->getPtr(),
        fd, what, event_callback, static_cast<void*>(event)) == 0);
}

static bool HHVM_METHOD(Event, setPriority, int64_t priority) {
  HPHP_EVENT_GET(event, this_, false);
  return (::event_priority_set(event->getPtr(), priority) == 0);
}

static bool HHVM_METHOD(Event, setTimer, const Object& baseObj,
    const Variant& cb, const Variant& arg/* = null_variant*/) {

  HPHP_EVENT_GET(event, this_, false);
  CHECK_PENDING(event, false);
  HPHP_EVENT_CHECK_CALLABLE(cb, false);
  HPHP_EVENT_BASE_GET(base, baseObj, false);

  event->set(cb, arg);

  return (::evtimer_assign(event->getPtr(), base->getPtr(),
        timer_callback, static_cast<void*>(event)) == 0);
}

static Object HHVM_STATIC_METHOD(Event, signal,
    const Object& baseObj, int64_t signum, const Variant& cb, const Variant& arg /* = null_variant */) {
  HPHP_EVENT_BASE_GET(base, baseObj, null_object);
  HPHP_EVENT_CHECK_CALLABLE(cb, null_object);

  auto rc = NEWOBJ(Event)(base->getPtr(),
      static_cast<evutil_socket_t>(signum),
      EV_SIGNAL | EV_PERSIST, cb, arg);
  return rc->wrap();
}

static Object HHVM_STATIC_METHOD(Event, timer,
    const Object& baseObj, const Variant& cb, const Variant& arg /* = null_variant */) {
  HPHP_EVENT_BASE_GET(base, baseObj, null_object);
  HPHP_EVENT_CHECK_CALLABLE(cb, null_object);

  auto rc = NEWOBJ(Event)(base->getPtr(), -1, EV_TIMEOUT | EV_PERSIST, cb, arg);
  return rc->wrap();
}

/////////////////////////////////////////////////////////////////////////

#define REG_INT_CONST(v) \
  Native::registerClassConstant<KindOfInt64>(s_Event.get(), \
      makeStaticString(#v), EV_ ## v)

#define REG_NAMED_INT_CONST(v, val) \
  Native::registerClassConstant<KindOfInt64>(s_Event.get(), \
      makeStaticString(#v), (val))

void EventExtension::_initEventClass() {
  REG_INT_CONST(ET);
  REG_INT_CONST(PERSIST);
  REG_INT_CONST(READ);
  REG_INT_CONST(WRITE);
  REG_INT_CONST(SIGNAL);
  REG_INT_CONST(TIMEOUT);
  REG_NAMED_INT_CONST(ALL_TYPES, EV_TIMEOUT | EV_READ | EV_WRITE | EV_SIGNAL | EV_PERSIST | EV_ET);

  HHVM_ME(Event, __constructor);
  HHVM_ME(Event, add);
  HHVM_ME(Event, del);
  HHVM_ME(Event, free);
  HHVM_STATIC_ME(Event, getSupportedMethods);
  HHVM_ME(Event, pending);
  HHVM_ME(Event, set);
  HHVM_ME(Event, setPriority);
  HHVM_ME(Event, setTimer);
  HHVM_STATIC_ME(Event, signal);
  HHVM_STATIC_ME(Event, timer);
}

#undef REG_INT_CONST
#undef REG_NAMED_INT_CONST

#undef SET_FD

} // namespace HPHP

// vim: et ts=2 sts=2 sw=2
