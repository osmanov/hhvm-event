/*
   +----------------------------------------------------------------------+
   | HipHop for PHP                                                       |
   +----------------------------------------------------------------------+
   | Copyright (c) 2010-2014 Facebook, Inc. (http://www.facebook.com)     |
   | Copyright (c) 1997-2014 The PHP Group                                |
   | Author: Ruslan Osmanov <osmanov@php.net>                             |
   +----------------------------------------------------------------------+
   | This source file is subject to version 3.01 of the PHP license,      |
   | that is bundled with this package in the file LICENSE, and is        |
   | available through the world-wide-web at the following url:           |
   | http://www.php.net/license/3_01.txt                                  |
   | If you did not receive a copy of the PHP license and are unable to   |
   | obtain it through the world-wide-web, please send a note to          |
   | license@php.net so we can mail you a copy immediately.               |
   +----------------------------------------------------------------------+
*/

#ifndef incl_HPHP_CLASSES_EVENT_H
#define incl_HPHP_CLASSES_EVENT_H

#include "ext_event.h"
#include "hphp/runtime/ext/ext_function.h"
#include "api/EventBase.h"

#define HPHP_EVENT_SET_FD(__fd, __fdvar, __ret)                \
do {                                                           \
  (__fd) = Event::varToFd((__fdvar));                          \
  if ((__fd) < 0) {                                            \
    raise_warning("invalid socket or file descriptor passed"); \
    return __ret;                                              \
  }                                                            \
} while (0)

namespace HPHP {

class Event: public EventResourceData {
  DECLARE_RESOURCE_ALLOCATION(Event);
  CLASSNAME_IS("Event");
  const String& o_getClassNameHook() const override { return classnameof(); }

  private:
    struct event* m_ptr = nullptr;
    Variant m_cb;
    Variant m_cbArg;

  public:
    Event();
    explicit Event(struct event_base* base, evutil_socket_t fd, short what, const Variant& cb, const Variant& arg);
    virtual ~Event();

    Object wrap();
    static Event* get(Object obj);
    bool isValid() const { return m_ptr; }

    static evutil_socket_t varToFd(const Variant& var);
    inline struct event* getPtr() const { return m_ptr; }
    inline void free();
    inline bool isPending();
    inline void set(const Variant& cb, const Variant& arg) {
      m_cb = cb;
      m_cbArg = arg;
    }
    inline void callback(evutil_socket_t fd, short what) const;
    inline void callback() const;
    inline void callback(evutil_socket_t fd) const;
};

} // namespace HPHP

#endif // incl_HPHP_CLASSES_EVENT_H

// vim: et ts=2 sts=2 sw=2
