/*
   +----------------------------------------------------------------------+
   | HipHop for PHP                                                       |
   +----------------------------------------------------------------------+
   | Copyright (c) 2010-2014 Facebook, Inc. (http://www.facebook.com)     |
   | Copyright (c) 1997-2014 The PHP Group                                |
   | Author: Ruslan Osmanov <osmanov@php.net>                             |
   +----------------------------------------------------------------------+
   | This source file is subject to version 3.01 of the PHP license,      |
   | that is bundled with this package in the file LICENSE, and is        |
   | available through the world-wide-web at the following url:           |
   | http://www.php.net/license/3_01.txt                                  |
   | If you did not receive a copy of the PHP license and are unable to   |
   | obtain it through the world-wide-web, please send a note to          |
   | license@php.net so we can mail you a copy immediately.               |
   +----------------------------------------------------------------------+
*/

#include "api/EventBuffer.h"

namespace HPHP {

/////////////////////////////////////////////////////////////////////////
// Internal

#define VAR_FALSE Variant(false)


EventBuffer::EventBuffer():
  m_ptr(::evbuffer_new()),
  m_owned(false)
{}

EventBuffer::EventBuffer(struct evbuffer* ptr):
  m_ptr(ptr), m_owned(true) {}


void EventBuffer::free() {
  if (m_ptr && m_owned == false) {
    ::evbuffer_free(m_ptr);
    m_ptr = nullptr;
  }
}


void EventBuffer::sweep() {
  free();
}


EventBuffer::~EventBuffer() {
  free();
}


EventBuffer* EventBuffer::get(Object obj) {
  return getInternalResource<EventBuffer>(obj, s_EventBuffer);
}

Object EventBuffer::wrap() {
  return newInstance(s_EventBuffer.get());
}

bool EventBuffer::setPos(struct evbuffer_ptr* ptr, const int64_t pos) {
  if (pos < 0) {
    return false;
  }

  if (::evbuffer_ptr_set(m_ptr, ptr, pos, ::EVBUFFER_PTR_SET) != -1) {
    return true;
  }

  raise_warning("Failed to set position to %ld", pos);
  return false;
}


/////////////////////////////////////////////////////////////////////////
// EventBuffer PHP class

static void HHVM_METHOD(EventBuffer, __construct) {
  auto rc = NEWOBJ(EventBuffer)();
  this_->o_set(s__rc, Resource(rc), s_EventBuffer.get());
}

static bool HHVM_METHOD(EventBuffer, add, const String& s) {
  HPHP_EVENT_BUFFER_GET(buf, this_, false);
  return (::evbuffer_add(buf->getPtr(), s.data(), s.length()) == 0);
}

static bool HHVM_METHOD(EventBuffer, addBuffer, const Object& srcObj) {
  HPHP_EVENT_BUFFER_GET(buf, this_, false);
  HPHP_EVENT_BUFFER_GET(src, srcObj, false);
  return (::evbuffer_add_buffer(buf->getPtr(), src->getPtr()) == 0);
}

static int64_t HHVM_METHOD(EventBuffer, getLength) {
  HPHP_EVENT_BUFFER_GET(buf, this_, 0);
  return ::evbuffer_get_length(buf->getPtr());
}

static int64_t HHVM_METHOD(EventBuffer, getContiguousSpace) {
  HPHP_EVENT_BUFFER_GET(buf, this_, 0);
  return ::evbuffer_get_contiguous_space(buf->getPtr());
}

static int64_t HHVM_METHOD(EventBuffer, appendFrom, const Object& srcObj, int64_t len) {
  HPHP_EVENT_BUFFER_GET(buf, this_, 0);
  HPHP_EVENT_BUFFER_GET(src, srcObj, 0);
  return ::evbuffer_remove_buffer(src->getPtr(), buf->getPtr(), static_cast<size_t>(len));
}

static int64_t HHVM_METHOD(EventBuffer, copyout, VRefParam sref, int64_t max_bytes) {
  HPHP_EVENT_BUFFER_GET(buf, this_, 0);

  char* data = new char[max_bytes + 1];
  int64_t ret = ::evbuffer_copyout(buf->getPtr(), data, max_bytes);

  if (ret > 0) {
    sref = String(data, ret, CopyString);
  } else {
    sref = String();
  }

  delete data;

  return ret;
}

static bool HHVM_METHOD(EventBuffer, drain, int64_t len) {
  HPHP_EVENT_BUFFER_GET(buf, this_, false);
  return (::evbuffer_drain(buf->getPtr(), len) == 0);
}

static void HHVM_METHOD(EventBuffer, enableLocking) {
  HPHP_EVENT_BUFFER_GET(buf, this_, /* void */);
  ::evbuffer_enable_locking(buf->getPtr(), NULL);
}

static bool HHVM_METHOD(EventBuffer, expand, int64_t len) {
  HPHP_EVENT_BUFFER_GET(buf, this_, false);
  return (::evbuffer_expand(buf->getPtr(), (size_t) len) == 0);
}

static bool HHVM_METHOD(EventBuffer, freeze, bool at_front) {
  HPHP_EVENT_BUFFER_GET(buf, this_, false);
  return (::evbuffer_freeze(buf->getPtr(), at_front) == 0);
}

static void HHVM_METHOD(EventBuffer, lock) {
  HPHP_EVENT_BUFFER_GET(buf, this_, /* void */);
  ::evbuffer_lock(buf->getPtr());
}

static bool HHVM_METHOD(EventBuffer, prepend, const String& data) {
  HPHP_EVENT_BUFFER_GET(buf, this_, false);
  return (::evbuffer_prepend(buf->getPtr(), data.data(), data.length()) == 0);
}

static bool HHVM_METHOD(EventBuffer, prependBuffer, const Object& srcObj) {
  HPHP_EVENT_BUFFER_GET(buf, this_, false);
  HPHP_EVENT_BUFFER_GET(src, srcObj, false);
  return (::evbuffer_prepend_buffer(buf->getPtr(), src->getPtr()) == 0);
}

static Variant HHVM_METHOD(EventBuffer, pullup, int64_t size) {
  HPHP_EVENT_BUFFER_GET(buf, this_, null_variant);

  unsigned char* mem = ::evbuffer_pullup(buf->getPtr(), size);
  if (mem == NULL) {
    return null_variant;
  }

  int64_t len = (int64_t) ::evbuffer_get_length(buf->getPtr());
  if (size >=0 && size < len) {
    len = size;
  }

  return Variant(String(reinterpret_cast<const char*>(mem), len, CopyString));
}


static Variant HHVM_METHOD(EventBuffer, read, int64_t max_bytes) {
  HPHP_EVENT_BUFFER_GET(buf, this_, false);

  if (max_bytes <= 0) {
    return VAR_FALSE;
  }

  char* data = (char *) malloc(sizeof(char) * max_bytes + 1);
  auto num = ::evbuffer_remove(buf->getPtr(), data, max_bytes);

  if (num > 0) {
    return String(data, num, AttachString);
  }
  return VAR_FALSE;
}

static Variant HHVM_METHOD(EventBuffer, readFrom, const Variant& fdvar, int64_t howmuch) {
  evutil_socket_t fd;

  HPHP_EVENT_BUFFER_GET(buf, this_, null_string);
  HPHP_EVENT_SET_FD(fd, fdvar, null_string);

  int64_t ret = ::evbuffer_read(buf->getPtr(), fd, howmuch);
  if (ret != -1) {
    return ret;
  }
  return VAR_FALSE;
}

static Variant HHVM_METHOD(EventBuffer, readLine, int64_t eol_style) {
  HPHP_EVENT_BUFFER_GET(buf, this_, VAR_FALSE);

  size_t len;
  char* res = ::evbuffer_readln(buf->getPtr(), &len, static_cast<evbuffer_eol_style>(eol_style));
  return res ? String(res, len, AttachString) : false;
}

static Variant HHVM_METHOD(EventBuffer, search, const String& what, int64_t start = -1, int64_t end = -1) {
  struct evbuffer_ptr ptr_start, ptr_end, ptr_res;

  HPHP_EVENT_BUFFER_GET(buf, this_, VAR_FALSE);

  if (start != -1 && buf->setPos(&ptr_start, start) == false) {
    start = -1;
  }
  if (end != -1 && (end > ::evbuffer_get_length(buf->getPtr()) ||
        buf->setPos(&ptr_end, end) == false)) {
    end = -1;
  }

  if (end != -1) {
    ptr_res = ::evbuffer_search_range(buf->getPtr(), what.c_str(), static_cast<size_t>(what.length()),
        (start != -1 ? &ptr_start : NULL), &ptr_end);
  } else {
    ptr_res = ::evbuffer_search(buf->getPtr(), what.c_str(), static_cast<size_t>(what.length()),
        (start != -1 ? &ptr_start : NULL));
  }

  if (ptr_res.pos != -1) {
    return ptr_res.pos;
  }
  return VAR_FALSE;
}

static Variant HHVM_METHOD(EventBuffer, searchEol, int64_t start = -1, int64_t eol_style = ::EVBUFFER_EOL_ANY) {
  struct evbuffer_ptr ptr_start, ptr_res;

  HPHP_EVENT_BUFFER_GET(buf, this_, VAR_FALSE);

  if (start != -1 && buf->setPos(&ptr_start, start) == false) {
    start = -1;
  }

  ptr_res = ::evbuffer_search_eol(buf->getPtr(),
      (start != -1 ? &ptr_start : NULL),
      NULL,
      static_cast<evbuffer_eol_style>(eol_style));

  if (ptr_res.pos != -1) {
    return ptr_res.pos;
  }
  return VAR_FALSE;
}

static Variant HHVM_METHOD(EventBuffer, substr, int64_t start, int64_t length = -1) {
  struct evbuffer_ptr    ptr;
  struct evbuffer_iovec* pv;
  int                    n_chunks;
  long                   n_read   = 0;
  int                    i;

  HPHP_EVENT_BUFFER_GET(buf, this_, VAR_FALSE);

  if (buf->setPos(&ptr, start) == false) {
    return VAR_FALSE;
  }

  // Determine how many chunks we need
  n_chunks = ::evbuffer_peek(buf->getPtr(), length, &ptr, NULL, 0);
  // Allocate space for the chunks
  pv = (struct evbuffer_iovec *) malloc(sizeof(struct evbuffer_iovec) * n_chunks);
  // Fill up pv
  n_chunks = ::evbuffer_peek(buf->getPtr(), length, &ptr, pv, n_chunks);

  // Determine the size of the result string
  for (i = 0; i < n_chunks; ++i) {
    size_t len = pv[i].iov_len;

    if (n_read + len > length) {
      len = length - n_read;
    }

    n_read += len;
  }

  String s = String(n_read + 1, ReserveString);
  char* ret = s.bufferSlice().ptr;

  for (n_read = 0, i = 0; i < n_chunks; ++i) {
    size_t len = pv[i].iov_len;

    if (n_read + len > length) {
      len = length - n_read;
    }

    ::memcpy(ret + n_read, pv[i].iov_base, len);

    n_read += len;
  }

  ::free(pv);

  ret[n_read] = '\0';
  s.setSize(n_read);

  return s;
}

static bool HHVM_METHOD(EventBuffer, unfreeze, bool at_front) {
  HPHP_EVENT_BUFFER_GET(buf, this_, false);
  return (::evbuffer_unfreeze(buf->getPtr(), at_front) == 0);
}

static void HHVM_METHOD(EventBuffer, unlock) {
  HPHP_EVENT_BUFFER_GET(buf, this_, /* void */);
  ::evbuffer_unlock(buf->getPtr());
}

static Variant HHVM_METHOD(EventBuffer, write, const Variant& fdvar, int64_t howmuch = -1) {
  int64_t res;
  evutil_socket_t fd;

  HPHP_EVENT_BUFFER_GET(buf, this_, VAR_FALSE);
  HPHP_EVENT_SET_FD(fd, fdvar, VAR_FALSE);

  res = howmuch < 0
    ? ::evbuffer_write(buf->getPtr(), fd)
    : ::evbuffer_write_atmost(buf->getPtr(), fd, howmuch);

  if (res == -1) {
    return VAR_FALSE;
  }
  return res;
}


/////////////////////////////////////////////////////////////////////////

#define REG_INT_CONST(v) \
  Native::registerClassConstant<KindOfInt64>(s_EventBuffer.get(), \
      makeStaticString(#v), EVBUFFER_ ## v)

void EventExtension::_initEventBufferClass() {
  REG_INT_CONST(EOL_ANY);
  REG_INT_CONST(EOL_CRLF);
  REG_INT_CONST(EOL_CRLF_STRICT);
  REG_INT_CONST(EOL_LF);
#if LIBEVENT_VERSION_NUMBER >= 0x02010100
  REG_INT_CONST(EOL_NUL);
#endif
  REG_INT_CONST(PTR_SET);
  REG_INT_CONST(PTR_ADD);

  HHVM_ME(EventBuffer, __construct);
  HHVM_ME(EventBuffer, add);
  HHVM_ME(EventBuffer, addBuffer);
  HHVM_ME(EventBuffer, getLength);
  HHVM_ME(EventBuffer, getContiguousSpace);
  HHVM_ME(EventBuffer, appendFrom);
  HHVM_ME(EventBuffer, copyout);
  HHVM_ME(EventBuffer, drain);
  HHVM_ME(EventBuffer, enableLocking);
  HHVM_ME(EventBuffer, expand);
  HHVM_ME(EventBuffer, freeze);
  HHVM_ME(EventBuffer, lock);
  HHVM_ME(EventBuffer, prepend);
  HHVM_ME(EventBuffer, prependBuffer);
  HHVM_ME(EventBuffer, pullup);
  HHVM_ME(EventBuffer, read);
  HHVM_ME(EventBuffer, readFrom);
  HHVM_ME(EventBuffer, readLine);
  HHVM_ME(EventBuffer, search);
  HHVM_ME(EventBuffer, searchEol);
  HHVM_ME(EventBuffer, substr);
  HHVM_ME(EventBuffer, unfreeze);
  HHVM_ME(EventBuffer, unlock);
  HHVM_ME(EventBuffer, write);
}

#undef REG_INT_CONST

#undef VAR_FALSE

} // namespace HPHP

// vim: et ts=2 sts=2 sw=2
