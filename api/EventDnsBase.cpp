/*
   +----------------------------------------------------------------------+
   | HipHop for PHP                                                       |
   +----------------------------------------------------------------------+
   | Copyright (c) 2010-2014 Facebook, Inc. (http://www.facebook.com)     |
   | Copyright (c) 1997-2014 The PHP Group                                |
   | Author: Ruslan Osmanov <osmanov@php.net>                             |
   +----------------------------------------------------------------------+
   | This source file is subject to version 3.01 of the PHP license,      |
   | that is bundled with this package in the file LICENSE, and is        |
   | available through the world-wide-web at the following url:           |
   | http://www.php.net/license/3_01.txt                                  |
   | If you did not receive a copy of the PHP license and are unable to   |
   | obtain it through the world-wide-web, please send a note to          |
   | license@php.net so we can mail you a copy immediately.               |
   +----------------------------------------------------------------------+
*/

#include "api/EventDnsBase.h"

namespace HPHP {

/////////////////////////////////////////////////////////////////////////
// Internal

void EventDnsBase::free() {
  if (m_ptr) {
    // Setting fail_requests to 1 makes all in-flight requests get
    // their callbacks invoked with a canceled error code before it
    // frees the base
    ::evdns_base_free(m_ptr, 1);
    m_ptr = nullptr;
  }
}


void EventDnsBase::sweep() {
  free();
}

EventDnsBase::EventDnsBase()
: m_ptr(nullptr) {}


EventDnsBase::EventDnsBase(struct event_base* base, bool initialize)
: m_ptr(::evdns_base_new(base, initialize)) {}


EventDnsBase::~EventDnsBase() {
  free();
}


EventDnsBase* EventDnsBase::get(Object obj) {
  return getInternalResource<EventDnsBase>(obj, s_EventDnsBase);
}


Object EventDnsBase::wrap() {
  return newInstance(s_EventDnsBase.get());
}


/////////////////////////////////////////////////////////////////////////
// EventDnsBase PHP class

static void HHVM_METHOD(EventDnsBase, __construct, const Object& baseObj, bool initialize) {
  HPHP_EVENT_BASE_GET(base, baseObj, /* void */);
  assert(base);

  auto rc = NEWOBJ(EventDnsBase)(base->getPtr(), initialize);
  this_->o_set(s__rc, Resource(rc), s_EventDnsBase.get());
}

static bool HHVM_METHOD(EventDnsBase, addNameserverIp, const String& ip) {
  HPHP_EVENT_DNSBASE_GET(dnsBase, this_, false);
  return (::evdns_base_nameserver_ip_add(dnsBase->getPtr(), ip.c_str()) == 0);
}

static void HHVM_METHOD(EventDnsBase, addSearch, const String& domain) {
  HPHP_EVENT_DNSBASE_GET(dnsBase, this_, /* void */);
  ::evdns_base_search_add(dnsBase->getPtr(), domain.c_str());
}

static void HHVM_METHOD(EventDnsBase, clearSearch) {
  HPHP_EVENT_DNSBASE_GET(dnsBase, this_, /* void */);
  ::evdns_base_search_clear(dnsBase->getPtr());
}

static int64_t HHVM_METHOD(EventDnsBase, countNameservers) {
  HPHP_EVENT_DNSBASE_GET(dnsBase, this_, 0);
  return (::evdns_base_count_nameservers(dnsBase->getPtr()));
}

static bool HHVM_METHOD(EventDnsBase, loadHosts, const String& hosts) {
  HPHP_EVENT_DNSBASE_GET(dnsBase, this_, false);
  return (::evdns_base_load_hosts(dnsBase->getPtr(), hosts.c_str()) == 0);
}

static bool HHVM_METHOD(EventDnsBase, parseResolvConf, int64_t flags, const String& filename) {
  if (flags & ~(DNS_OPTION_NAMESERVERS | DNS_OPTION_SEARCH | DNS_OPTION_MISC
        | DNS_OPTIONS_ALL)) {
    raise_warning("Invalid flags");
    return false;
  }

  HPHP_EVENT_DNSBASE_GET(dnsBase, this_, false);

  int64_t ret = ::evdns_base_resolv_conf_parse(dnsBase->getPtr(), flags, filename.c_str());

  if (ret) {
    char err[40];

    switch (ret) {
      case 1:
        strcpy(err, "Failed to open file");
        break;
      case 2:
        strcpy(err, "Failed to stat file");
        break;
      case 3:
        strcpy(err, "File too large");
        break;
      case 4:
        strcpy(err, "Out of memory");
        break;
      case 5:
        strcpy(err, "Short read from file");
        break;
      case 6:
        strcpy(err, "No nameservers listed in the file");
        break;
    }

    raise_warning("%s", err);
  }

  return true;
}

static bool HHVM_METHOD(EventDnsBase, setOption, const String& option, const String& value) {
  HPHP_EVENT_DNSBASE_GET(dnsBase, this_, false);
  return (::evdns_base_set_option(dnsBase->getPtr(), option.c_str(), value.c_str()) == 0);
}

static void HHVM_METHOD(EventDnsBase, setSearchNdots, int64_t ndots) {
  HPHP_EVENT_DNSBASE_GET(dnsBase, this_, /* void */);
  ::evdns_base_search_ndots_set(dnsBase->getPtr(), ndots);
}

/////////////////////////////////////////////////////////////////////////


#define REG_INT_CONST(v) \
  Native::registerClassConstant<KindOfInt64>(s_EventDnsBase.get(), \
      makeStaticString(#v), DNS_ ## v)

void EventExtension::_initEventDnsClass() {
  HHVM_ME(EventDnsBase, __construct);
  HHVM_ME(EventDnsBase, addNameserverIp);
  HHVM_ME(EventDnsBase, addSearch);
  HHVM_ME(EventDnsBase, clearSearch);
  HHVM_ME(EventDnsBase, countNameservers);
  HHVM_ME(EventDnsBase, loadHosts);
  HHVM_ME(EventDnsBase, parseResolvConf);
  HHVM_ME(EventDnsBase, setOption);
  HHVM_ME(EventDnsBase, setSearchNdots);

  REG_INT_CONST(OPTION_SEARCH);
  REG_INT_CONST(OPTION_NAMESERVERS);
  REG_INT_CONST(OPTION_MISC);
  REG_INT_CONST(OPTION_HOSTSFILE);
  REG_INT_CONST(OPTIONS_ALL);
}

#undef REG_INT_CONST

} // namespace HPHP

// vim: et ts=2 sts=2 sw=2
