/*
   +----------------------------------------------------------------------+
   | HipHop for PHP                                                       |
   +----------------------------------------------------------------------+
   | Copyright (c) 2010-2014 Facebook, Inc. (http://www.facebook.com)     |
   | Copyright (c) 1997-2014 The PHP Group                                |
   | Author: Ruslan Osmanov <osmanov@php.net>                             |
   +----------------------------------------------------------------------+
   | This source file is subject to version 3.01 of the PHP license,      |
   | that is bundled with this package in the file LICENSE, and is        |
   | available through the world-wide-web at the following url:           |
   | http://www.php.net/license/3_01.txt                                  |
   | If you did not receive a copy of the PHP license and are unable to   |
   | obtain it through the world-wide-web, please send a note to          |
   | license@php.net so we can mail you a copy immediately.               |
   +----------------------------------------------------------------------+
*/

#ifndef incl_HPHP_CLASSES_EVENT_BUFFER_EVENT_H
#define incl_HPHP_CLASSES_EVENT_BUFFER_EVENT_H

#include "ext_event.h"
#include "api/Event.h"
#include "api/EventBuffer.h"
#include "api/EventDnsBase.h"
#include "api/EventSslContext.h"

namespace HPHP {

#if LIBEVENT_VERSION_NUMBER < 0x02000200
// These types introduced in libevent 2.0.2-alpha
typedef void (*bufferevent_data_cb)(struct bufferevent* bev, void* ctx);
typedef void (*bufferevent_event_cb)(struct bufferevent* bev, short events, void* ctx);
#endif

class EventBufferEvent: public EventResourceData {
  DECLARE_RESOURCE_ALLOCATION(EventBufferEvent);
  CLASSNAME_IS("EventBufferEvent");
  const String& o_getClassNameHook() const override { return classnameof(); }

  private:
    struct bufferevent* m_ptr = nullptr;
    Variant m_readCb;
    Variant m_writeCb;
    Variant m_eventCb;
    Variant m_cbArg;

  public:
    EventBufferEvent();
    explicit EventBufferEvent(const Object& baseObj, const Variant& socketvar, int64_t options,
        const Variant& readCbVar, const Variant& writeCbVar, const Variant& eventCbVar, const Variant& arg);
    explicit EventBufferEvent(struct event_base* base, struct bufferevent* bevent,
        SSL_CTX* ctx, int64_t state, int64_t options);
    explicit EventBufferEvent(struct event_base* base, evutil_socket_t fd,
        SSL_CTX* ctx, int64_t state, int64_t options);
    virtual ~EventBufferEvent();
    inline void free();

    Object getInputBuffer();
    Object getOutputBuffer();
    Object wrap();
    static EventBufferEvent* get(Object obj);
    bool isValid() const { return m_ptr; }
    inline struct bufferevent* getPtr() const { return m_ptr; }

    void readCallback() const;
    void writeCallback() const;
    void eventCallback(int64_t events) const;

    inline void setCallbacks(const Variant& rcb, const Variant& wcb, const Variant& ecb, const Variant& arg) {
      m_readCb  = rcb;
      m_writeCb = wcb;
      m_eventCb = ecb;
      m_cbArg   = arg;
    }

    inline static bool isValidSslState(int64_t state) {
      return (state == ::BUFFEREVENT_SSL_OPEN
          || state == ::BUFFEREVENT_SSL_CONNECTING
          || state == ::BUFFEREVENT_SSL_ACCEPTING);
    }
};

} // namespace HPHP

#endif // incl_HPHP_CLASSES_EVENT_BUFFER_EVENT_H

// vim: et ts=2 sts=2 sw=2
