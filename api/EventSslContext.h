/*
   +----------------------------------------------------------------------+
   | HipHop for PHP                                                       |
   +----------------------------------------------------------------------+
   | Copyright (c) 2010-2014 Facebook, Inc. (http://www.facebook.com)     |
   | Copyright (c) 1997-2014 The PHP Group                                |
   | Author: Ruslan Osmanov <osmanov@php.net>                             |
   +----------------------------------------------------------------------+
   | This source file is subject to version 3.01 of the PHP license,      |
   | that is bundled with this package in the file LICENSE, and is        |
   | available through the world-wide-web at the following url:           |
   | http://www.php.net/license/3_01.txt                                  |
   | If you did not receive a copy of the PHP license and are unable to   |
   | obtain it through the world-wide-web, please send a note to          |
   | license@php.net so we can mail you a copy immediately.               |
   +----------------------------------------------------------------------+
*/

#ifndef incl_HPHP_CLASSES_EVENT_SSLCONTEXT_H
#define incl_HPHP_CLASSES_EVENT_SSLCONTEXT_H

#include "ext_event.h"

#ifdef HPHP_DISABLE_EVENT_OPENSSL
# error __FILE__ must not be included because OpenSSL is disabled
#endif

namespace HPHP {

class EventSslContext: public EventResourceData {
  DECLARE_RESOURCE_ALLOCATION(EventSslContext);
  CLASSNAME_IS("EventSslContext");
  const String& o_getClassNameHook() const override { return classnameof(); }

  private:
    SSL_CTX* m_ptr = nullptr;

  public:
    EventSslContext();
    virtual ~EventSslContext();

    static EventSslContext* get(Object obj);

    bool isValid() const { return m_ptr; }
    inline SSL_CTX* getPtr() const { return m_ptr; }
    inline void free();
};

} // namespace HPHP

#endif // incl_HPHP_CLASSES_EVENT_SSLCONTEXT_H

// vim: et ts=2 sts=2 sw=2
