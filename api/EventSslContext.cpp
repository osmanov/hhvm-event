/*
   +----------------------------------------------------------------------+
   | HipHop for PHP                                                       |
   +----------------------------------------------------------------------+
   | Copyright (c) 2010-2014 Facebook, Inc. (http://www.facebook.com)     |
   | Copyright (c) 1997-2014 The PHP Group                                |
   | Author: Ruslan Osmanov <osmanov@php.net>                             |
   +----------------------------------------------------------------------+
   | This source file is subject to version 3.01 of the PHP license,      |
   | that is bundled with this package in the file LICENSE, and is        |
   | available through the world-wide-web at the following url:           |
   | http://www.php.net/license/3_01.txt                                  |
   | If you did not receive a copy of the PHP license and are unable to   |
   | obtain it through the world-wide-web, please send a note to          |
   | license@php.net so we can mail you a copy immediately.               |
   +----------------------------------------------------------------------+
*/

#include "api/EventSslContext.h"

namespace HPHP {

/////////////////////////////////////////////////////////////////////////
// Internal

void EventSslContext::free() {
  if (m_ptr) {
    ::SSL_CTX_free(m_ptr);
    m_ptr = nullptr;
  }
}


void EventSslContext::sweep() {
  free();
}


EventSslContext::EventSslContext():
  m_ptr(nullptr) {}


EventSslContext::~EventSslContext() {
  free();
}


EventSslContext* EventSslContext::get(Object obj) {
  return getInternalResource<EventSslContext>(obj, s_EventSslContext);
}


/////////////////////////////////////////////////////////////////////////
// EventSslContext PHP class

/////////////////////////////////////////////////////////////////////////

#define REG_INT_CONST(v, n) \
  Native::registerClassConstant<KindOfInt64>(s_EventSslContext.get(), \
      makeStaticString(#v), n)

void EventExtension::_initEventSslContextClass() {

}

#undef REG_INT_CONST

} // namespace HPHP

// vim: et ts=2 sts=2 sw=2
