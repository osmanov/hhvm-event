/*
   +----------------------------------------------------------------------+
   | HipHop for PHP                                                       |
   +----------------------------------------------------------------------+
   | Copyright (c) 2010-2014 Facebook, Inc. (http://www.facebook.com)     |
   | Copyright (c) 1997-2014 The PHP Group                                |
   | Author: Ruslan Osmanov <osmanov@php.net>                             |
   +----------------------------------------------------------------------+
   | This source file is subject to version 3.01 of the PHP license,      |
   | that is bundled with this package in the file LICENSE, and is        |
   | available through the world-wide-web at the following url:           |
   | http://www.php.net/license/3_01.txt                                  |
   | If you did not receive a copy of the PHP license and are unable to   |
   | obtain it through the world-wide-web, please send a note to          |
   | license@php.net so we can mail you a copy immediately.               |
   +----------------------------------------------------------------------+
*/

#ifndef incl_HPHP_CLASSES_EVENT_BASE_H
#define incl_HPHP_CLASSES_EVENT_BASE_H

#include "ext_event.h"
#include "api/EventConfig.h"
#include "src/EventRequestData.h"

namespace HPHP {

class EventBase: public EventResourceData {
  DECLARE_RESOURCE_ALLOCATION(EventBase);
  CLASSNAME_IS("EventBase");
  const String& o_getClassNameHook() const override { return classnameof(); }

  protected:
    struct event_base* m_ptr = nullptr;
    // RequestLocal implements some operators `->` and `*`,
    // which call RequestLocal::get() method. The `get()` method, in turn, (lazily) calls
    // `requestInit()` and registers `requestShutdown()`.
    // So the first time we access the request-local variable, we implicitly trigger
    // `requestInit()`.
    inline void _initRequestData();
    inline void _free();

  public:
    EventBase();
    EventBase(struct event_config* cfg);
    virtual ~EventBase();

    DECLARE_STATIC_REQUEST_LOCAL(EventRequestData, m_tlsRequestData);

    static EventBase* get(Object obj);

    bool isValid() const { return m_ptr; }
    inline struct event_base* getPtr() const { return m_ptr; }
};

} // namespace HPHP

#endif // incl_HPHP_CLASSES_EVENT_BASE_H

// vim: et ts=2 sts=2 sw=2
