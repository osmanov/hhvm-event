/*
   +----------------------------------------------------------------------+
   | HipHop for PHP                                                       |
   +----------------------------------------------------------------------+
   | Copyright (c) 2010-2014 Facebook, Inc. (http://www.facebook.com)     |
   | Copyright (c) 1997-2014 The PHP Group                                |
   | Author: Ruslan Osmanov <osmanov@php.net>                             |
   +----------------------------------------------------------------------+
   | This source file is subject to version 3.01 of the PHP license,      |
   | that is bundled with this package in the file LICENSE, and is        |
   | available through the world-wide-web at the following url:           |
   | http://www.php.net/license/3_01.txt                                  |
   | If you did not receive a copy of the PHP license and are unable to   |
   | obtain it through the world-wide-web, please send a note to          |
   | license@php.net so we can mail you a copy immediately.               |
   +----------------------------------------------------------------------+
*/

#include "api/EventBase.h"
#include "src/EventRequestData.h"

namespace HPHP {

/////////////////////////////////////////////////////////////////////////
// Internal

IMPLEMENT_REQUEST_LOCAL(EventRequestData, EventBase::m_tlsRequestData);


void EventBase::_initRequestData() {
  // Trigger request init handler to setup/configure Libevent and OpenSSL.
  // ```
  // Before using any of the functions in the library, you must call
  // event_init() or event_base_new() to perform one-time initialization of the
  // libevent library.
  // ```
  //    See <http://www.wangafu.net/~nickm/libevent-2.0/doxygen/html/>.
  //
  // Thus, no Event API could be run until an EventBase object is instantiated.
  // So we can be sure that Libevent and OpenSSL are initialized.
  m_tlsRequestData.get();
}


EventBase::EventBase() {
  _initRequestData();

  m_ptr = ::event_base_new();
  if (!m_ptr) {
    raise_error("event_base_new failed");
  }
}


EventBase::EventBase(struct event_config* cfg) {
  _initRequestData();

  assert(cfg);
  m_ptr = ::event_base_new_with_config(cfg);
  if (!m_ptr) {
    raise_error("event_base_new_with_config failed");
  }
}


void EventBase::_free() {
  if (m_ptr) {
    ::event_base_free(m_ptr);
    m_ptr = nullptr;
  }
}


void EventBase::sweep()
{
  _free();
}


EventBase::~EventBase() {
  _free();
}

EventBase* EventBase::get(Object obj) {
  return getInternalResource<EventBase>(obj, s_EventBase);
}

/////////////////////////////////////////////////////////////////////////
// EventBase PHP class

static void HHVM_METHOD(EventBase, __construct, const Variant& cfg /* = null_variant */) {
  if (cfg.isNull()) {
    auto rc = NEWOBJ(EventBase)();
    this_->o_set(s__rc, Resource(rc), s_EventBase.get());
  } else {
    HPHP_EVENT_CONFIG_GET(rc_cfg, cfg.toObject(), /* void */);
    auto rc = NEWOBJ(EventBase)(rc_cfg->getPtr());
    this_->o_set(s__rc, Resource(rc), s_EventBase.get());
  }
}

static bool HHVM_METHOD(EventBase, dispatch) {
  JIT::VMRegAnchor _;
  HPHP_EVENT_BASE_GET(rc, this_, false);
  return (::event_base_dispatch(rc->getPtr()) != -1);
}

static bool HHVM_METHOD(EventBase, exitLoop, double timeout = -1.0) {
  HPHP_EVENT_BASE_GET(rc, this_, false);

  if (timeout == -1.0) {
    return (::event_base_loopexit(rc->getPtr(), NULL) == 0);
  }

  struct timeval tv;
  HPHP_EVENT_TIMEVAL_SET(tv, timeout);
  return (::event_base_loopexit(rc->getPtr(), &tv) == 0);
}

static int64_t HHVM_METHOD(EventBase, getFeatures) {
  HPHP_EVENT_BASE_GET(rc, this_, 0);
  return (::event_base_get_features(rc->getPtr()));
}

static String HHVM_METHOD(EventBase, getMethod) {
  HPHP_EVENT_BASE_GET(rc, this_, null_string);
  return String(::event_base_get_method(rc->getPtr()));
}

static double HHVM_METHOD(EventBase, getTimeOfDayCached) {
  struct timeval tv;

  HPHP_EVENT_BASE_GET(rc, this_, 0.0);
  return (::event_base_gettimeofday_cached(rc->getPtr(), &tv)
      ? 0.0 : HPHP_EVENT_TIMEVAL_TO_DOUBLE(tv));
}

static bool HHVM_METHOD(EventBase, gotExit) {
  HPHP_EVENT_BASE_GET(rc, this_, false);
  return (::event_base_got_exit(rc->getPtr()));
}

static bool HHVM_METHOD(EventBase, gotStop) {
  HPHP_EVENT_BASE_GET(rc, this_, false);
  return (::event_base_got_break(rc->getPtr()));
}

static bool HHVM_METHOD(EventBase, loop, int64_t flags = -1) {
  JIT::VMRegAnchor _;

  HPHP_EVENT_BASE_GET(rc, this_, false);

  if (flags == -1) {
    if (::event_base_dispatch(rc->getPtr()) == -1) {
      return false;
    }
  } else if (::event_base_loop(rc->getPtr(), flags) == -1) {
    return false;
  }
  return true;
}

static bool HHVM_METHOD(EventBase, priorityInit, int64_t priorities) {
  HPHP_EVENT_BASE_GET(rc, this_, false);
  return (::event_base_priority_init(rc->getPtr(), priorities) == 0);
}

static bool HHVM_METHOD(EventBase, reInit) {
  HPHP_EVENT_BASE_GET(rc, this_, false);
  return (::event_reinit(rc->getPtr()) == 0);
}

static bool HHVM_METHOD(EventBase, stop) {
  HPHP_EVENT_BASE_GET(rc, this_, false);
  return (::event_base_loopbreak(rc->getPtr()) == 0);
}


/////////////////////////////////////////////////////////////////////////

#define REG_INT_CONST(v, vorg) \
  Native::registerClassConstant<KindOfInt64>(s_EventBase.get(), \
      makeStaticString(#v), (vorg))

void EventExtension::_initEventBaseClass() {
  REG_INT_CONST(LOOP_ONCE, EVLOOP_ONCE);
  REG_INT_CONST(LOOP_NONBLOCK, EVLOOP_NONBLOCK);

  // Runtime flags of event base usually passed to event_config_set_flag
  REG_INT_CONST(NOLOCK, EVENT_BASE_FLAG_NOLOCK);
  REG_INT_CONST(STARTUP_IOCP, EVENT_BASE_FLAG_STARTUP_IOCP);
  REG_INT_CONST(NO_CACHE_TIME, EVENT_BASE_FLAG_NO_CACHE_TIME);
  REG_INT_CONST(EPOLL_USE_CHANGELIST, EVENT_BASE_FLAG_EPOLL_USE_CHANGELIST);
#ifdef EVENT_BASE_FLAG_IGNORE_ENV
  REG_INT_CONST(IGNORE_ENV, IGNORE_ENV);
#endif
#ifdef EVENT_BASE_FLAG_PRECISE_TIMER
  REG_INT_CONST(PRECISE_TIMER, PRECISE_TIMER);
#endif

  HHVM_ME(EventBase, __construct);
  HHVM_ME(EventBase, dispatch);
  HHVM_ME(EventBase, exitLoop);
  HHVM_ME(EventBase, getFeatures);
  HHVM_ME(EventBase, getMethod);
  HHVM_ME(EventBase, getTimeOfDayCached);
  HHVM_ME(EventBase, gotExit);
  HHVM_ME(EventBase, gotStop);
  HHVM_ME(EventBase, loop);
  HHVM_ME(EventBase, priorityInit);
  HHVM_ME(EventBase, reInit);
  HHVM_ME(EventBase, stop);
}

#undef REG_INT_CONST

} // namespace HPHP

// vim: et ts=2 sts=2 sw=2
