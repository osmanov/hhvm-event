/*
   +----------------------------------------------------------------------+
   | HipHop for PHP                                                       |
   +----------------------------------------------------------------------+
   | Copyright (c) 2010-2014 Facebook, Inc. (http://www.facebook.com)     |
   | Copyright (c) 1997-2014 The PHP Group                                |
   | Author: Ruslan Osmanov <osmanov@php.net>                             |
   +----------------------------------------------------------------------+
   | This source file is subject to version 3.01 of the PHP license,      |
   | that is bundled with this package in the file LICENSE, and is        |
   | available through the world-wide-web at the following url:           |
   | http://www.php.net/license/3_01.txt                                  |
   | If you did not receive a copy of the PHP license and are unable to   |
   | obtain it through the world-wide-web, please send a note to          |
   | license@php.net so we can mail you a copy immediately.               |
   +----------------------------------------------------------------------+
*/

#include "api/EventConfig.h"

namespace HPHP {

/////////////////////////////////////////////////////////////////////////
// Internal

void EventConfig::free() {
  if (m_ptr) {
    ::event_config_free(m_ptr);
    m_ptr = nullptr;
  }
}


void EventConfig::sweep() {
  free();
}


EventConfig::EventConfig() {
  m_ptr = ::event_config_new();
  if (!m_ptr) {
    raise_error("event_config_new failed");
  }
}


EventConfig::~EventConfig() {
  free();
}


EventConfig* EventConfig::get(Object obj) {
  return getInternalResource<EventConfig>(obj, s_EventConfig);
}


/////////////////////////////////////////////////////////////////////////
// EventConfig PHP class

static void HHVM_METHOD(EventConfig, __construct) {
  auto rc = NEWOBJ(EventConfig)();
  this_->o_set(s__rc, Resource(rc), s_EventConfig.get());
}

static bool HHVM_METHOD(EventConfig, avoidMethod, const String& method) {
  HPHP_EVENT_CONFIG_GET(rc, this_, false);
  return (::event_config_avoid_method(rc->getPtr(), method.c_str()) == 0);
}

static bool HHVM_METHOD(EventConfig, requireFeatures, int64_t features) {
  HPHP_EVENT_CONFIG_GET(rc, this_, false);
  return (::event_config_require_features(rc->getPtr(), features) == 0);
}

static void HHVM_METHOD(EventConfig, setMaxDispatchInterval,
    int64_t max_interval, int64_t max_callbacks, int64_t min_priority) {
#if LIBEVENT_VERSION_NUMBER >= 0x02010000
  HPHP_EVENT_CONFIG_GET(rc, this_, /* void */);

  if (max_interval > 0) {
    struct timeval tv;
    HPHP_EVENT_TIMEVAL_SET(tv, max_interval);
    ::event_config_set_max_dispatch_interval(rc->getPtr(), &tv, max_callbacks, min_priority);
  } else {
    ::event_config_set_max_dispatch_interval(rc->getPtr(), NULL, max_callbacks, min_priority);
  }
#endif // LIBEVENT_VERSION_NUMBER >= 0x02010000
}

/////////////////////////////////////////////////////////////////////////

#define REG_INT_CONST(v) \
  Native::registerClassConstant<KindOfInt64>(s_EventConfig.get(), \
      makeStaticString(#v), EV_ ## v)

void EventExtension::_initEventConfigClass() {
  REG_INT_CONST(FEATURE_ET);
  REG_INT_CONST(FEATURE_O1);
  REG_INT_CONST(FEATURE_FDS);

  HHVM_ME(EventConfig, __construct);
  HHVM_ME(EventConfig, avoidMethod);
  HHVM_ME(EventConfig, requireFeatures);
  HHVM_ME(EventConfig, setMaxDispatchInterval);
}

#undef REG_INT_CONST

} // namespace HPHP

// vim: et ts=2 sts=2 sw=2
