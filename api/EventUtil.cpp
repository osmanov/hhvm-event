/*
   +----------------------------------------------------------------------+
   | HipHop for PHP                                                       |
   +----------------------------------------------------------------------+
   | Copyright (c) 2010-2014 Facebook, Inc. (http://www.facebook.com)     |
   | Copyright (c) 1997-2014 The PHP Group                                |
   | Author: Ruslan Osmanov <osmanov@php.net>                             |
   +----------------------------------------------------------------------+
   | This source file is subject to version 3.01 of the PHP license,      |
   | that is bundled with this package in the file LICENSE, and is        |
   | available through the world-wide-web at the following url:           |
   | http://www.php.net/license/3_01.txt                                  |
   | If you did not receive a copy of the PHP license and are unable to   |
   | obtain it through the world-wide-web, please send a note to          |
   | license@php.net so we can mail you a copy immediately.               |
   +----------------------------------------------------------------------+
*/

#include "api/EventUtil.h"

namespace HPHP {

/////////////////////////////////////////////////////////////////////////
// Internal



/////////////////////////////////////////////////////////////////////////
// EventUtil PHP class

static int64_t HHVM_STATIC_METHOD(EventUtil, getLastSocketErrno, const Variant& socket/* = null_variant*/) {
  if (!socket.isNull()) {
    evutil_socket_t fd;
    HPHP_EVENT_SET_FD(fd, socket, 0);
    return evutil_socket_geterror(fd);
  }
  return EVUTIL_SOCKET_ERROR();
}


/////////////////////////////////////////////////////////////////////////

#define REG_INT_CONST(v, n) \
  Native::registerClassConstant<KindOfInt64>(s_EventUtil.get(), \
      makeStaticString(#v), (n))

void EventExtension::_initEventUtilClass() {
  HHVM_STATIC_ME(EventUtil, getLastSocketErrno);

  // Address families
  REG_INT_CONST(AF_INET, AF_INET);
  REG_INT_CONST(AF_INET6, AF_INET6);
  REG_INT_CONST(AF_UNIX, AF_UNIX);
  REG_INT_CONST(AF_UNSPEC, AF_UNSPEC);

  // Socket options
  REG_INT_CONST(SO_DEBUG, SO_DEBUG);
  REG_INT_CONST(SO_REUSEADDR, SO_REUSEADDR);
  REG_INT_CONST(SO_KEEPALIVE, SO_KEEPALIVE);
  REG_INT_CONST(SO_DONTROUTE, SO_DONTROUTE);
  REG_INT_CONST(SO_LINGER, SO_LINGER);
  REG_INT_CONST(SO_BROADCAST, SO_BROADCAST);
  REG_INT_CONST(SO_OOBINLINE, SO_OOBINLINE);
  REG_INT_CONST(SO_SNDBUF, SO_SNDBUF);
  REG_INT_CONST(SO_RCVBUF, SO_RCVBUF);
  REG_INT_CONST(SO_SNDLOWAT, SO_SNDLOWAT);
  REG_INT_CONST(SO_RCVLOWAT, SO_RCVLOWAT);
  REG_INT_CONST(SO_SNDTIMEO, SO_SNDTIMEO);
  REG_INT_CONST(SO_RCVTIMEO, SO_RCVTIMEO);
  REG_INT_CONST(SO_TYPE, SO_TYPE);
  REG_INT_CONST(SO_ERROR, SO_ERROR);
#ifdef TCP_NODELAY
  REG_INT_CONST(TCP_NODELAY, TCP_NODELAY);
#endif

  // Socket protocol levels
  REG_INT_CONST(SOL_SOCKET, SOL_SOCKET);
  REG_INT_CONST(SOL_TCP,    IPPROTO_TCP);
  REG_INT_CONST(SOL_UDP,    IPPROTO_UDP);
  REG_INT_CONST(IPPROTO_IP, IPPROTO_IP);
#if HAVE_IPV6
  REG_INT_CONST(IPPROTO_IPV6, IPPROTO_IPV6);
#endif

}

#undef REG_INT_CONST
#undef REG_NAMED_INT_CONST

} // namespace HPHP

// vim: et ts=2 sts=2 sw=2
