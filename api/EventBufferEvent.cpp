/*
   +----------------------------------------------------------------------+
   | HipHop for PHP                                                       |
   +----------------------------------------------------------------------+
   | Copyright (c) 2010-2014 Facebook, Inc. (http://www.facebook.com)     |
   | Copyright (c) 1997-2014 The PHP Group                                |
   | Author: Ruslan Osmanov <osmanov@php.net>                             |
   +----------------------------------------------------------------------+
   | This source file is subject to version 3.01 of the PHP license,      |
   | that is bundled with this package in the file LICENSE, and is        |
   | available through the world-wide-web at the following url:           |
   | http://www.php.net/license/3_01.txt                                  |
   | If you did not receive a copy of the PHP license and are unable to   |
   | obtain it through the world-wide-web, please send a note to          |
   | license@php.net so we can mail you a copy immediately.               |
   +----------------------------------------------------------------------+
*/

#include "api/EventBufferEvent.h"
#include "src/EventRequestData.h"
#include "api/EventBase.h"

namespace HPHP {

/////////////////////////////////////////////////////////////////////////
// Internal

#define VAR_FALSE Variant(false)

#define HPHP_EVENT_OPENSSL_REQUIRED_ERROR(__f, __ret)   \
do {                                                    \
  raise_error(#__f " requires OpenSSL support. "        \
      "Rebuild event extension with OpenSSL support."); \
  return __ret;                                         \
} while (0)


#if 0
IMPLEMENT_OBJECT_ALLOCATION(EventBufferEvent);
#endif



static void bevent_read_cb(struct bufferevent* bevent, void* ptr) {
  assert(ptr);
  EventBufferEvent* rc = static_cast<EventBufferEvent*>(ptr);
  assert(!rc->isInvalid());
  rc->readCallback();
}

static void bevent_write_cb(struct bufferevent* bevent, void* ptr) {
  assert(ptr);
  EventBufferEvent* rc = static_cast<EventBufferEvent*>(ptr);
  assert(!rc->isInvalid());
  rc->writeCallback();
}

static void bevent_event_cb(struct bufferevent* bevent, short events, void* ptr) {
  assert(ptr);
  EventBufferEvent* rc = static_cast<EventBufferEvent*>(ptr);
  assert(!rc->isInvalid());
  rc->eventCallback(events);
}


void EventBufferEvent::readCallback() const {
  assert(m_ptr);
  if (!m_readCb.isNull()) {
#ifndef HPHP_DISABLE_EVENT_PTHREADS
    ::bufferevent_lock(m_ptr);
#endif

    vm_call_user_func(m_readCb, make_packed_array(m_cbArg));

#ifndef HPHP_DISABLE_EVENT_PTHREADS
    ::bufferevent_unlock(m_ptr);
#endif
  }
}

void EventBufferEvent::writeCallback() const {
  assert(m_ptr);
  if (!m_writeCb.isNull()) {
#ifndef HPHP_DISABLE_EVENT_PTHREADS
    ::bufferevent_lock(m_ptr);
#endif

    vm_call_user_func(m_writeCb, make_packed_array(m_cbArg));

#ifndef HPHP_DISABLE_EVENT_PTHREADS
    ::bufferevent_unlock(m_ptr);
#endif
  }
}

void EventBufferEvent::eventCallback(int64_t events) const {
  assert(m_ptr);
  if (!m_eventCb.isNull()) {
#ifndef HPHP_DISABLE_EVENT_PTHREADS
    ::bufferevent_lock(m_ptr);
#endif

    vm_call_user_func(m_eventCb, make_packed_array(events, m_cbArg));

#ifndef HPHP_DISABLE_EVENT_PTHREADS
    ::bufferevent_unlock(m_ptr);
#endif
  }
}

EventBufferEvent::EventBufferEvent():
  m_ptr(nullptr),
  m_readCb(null_variant),
  m_writeCb(null_variant),
  m_eventCb(null_variant),
  m_cbArg(null_variant)
{}

EventBufferEvent::EventBufferEvent(const Object& baseObj, const Variant& socketVar, int64_t options,
    const Variant& readCbVar, const Variant& writeCbVar, const Variant& eventCbVar, const Variant& arg):
  m_ptr(nullptr),
  m_readCb(readCbVar),
  m_writeCb(writeCbVar),
  m_eventCb(eventCbVar),
  m_cbArg(arg)
{
    HPHP_EVENT_BASE_GET(base, baseObj, /* void */);

    evutil_socket_t      fd;
    bufferevent_data_cb  readCb;
    bufferevent_data_cb  writeCb;
    bufferevent_event_cb eventCb;

    if (!socketVar.isNull()) {
      HPHP_EVENT_SET_FD(fd, socketVar, /* void */);
      // Make sure that the socket is in non-blocking mode(libevent's tip)
      ::evutil_make_socket_nonblocking(fd);
    } else {
      // User decided to assign fd later,
      // e.g. by means of bufferevent_socket_connect()
      // which allocates new socket stream in this case.
      fd = -1;
      // User has no access to the file descriptor created
      // internally (bufferevent_getfd is not exposed to userspace at the
      // moment). Therefore, we have to make it close-on-free.
      options |= ::BEV_OPT_CLOSE_ON_FREE;
    }

#ifndef HPHP_DISABLE_EVENT_PTHREADS
    options |= ::BEV_OPT_THREADSAFE;
#endif
    m_ptr = ::bufferevent_socket_new(base->getPtr(), fd, options);
    if (m_ptr == NULL) {
      raise_error("Failed to allocate bufferevent for socket");
      return;
    }

    readCb  = f_is_callable(readCbVar)  ? bevent_read_cb  : NULL;
    writeCb = f_is_callable(writeCbVar) ? bevent_write_cb : NULL;
    eventCb = f_is_callable(eventCbVar) ? bevent_event_cb : NULL;

    if (readCb || writeCb || eventCb || !arg.isNull()) {
      ::bufferevent_setcb(m_ptr, readCb, writeCb, eventCb, static_cast<void*>(this));
    }
}

#ifndef HPHP_DISABLE_EVENT_OPENSSL
EventBufferEvent::EventBufferEvent(struct event_base* base,
    struct bufferevent* bevent, SSL_CTX* ctx,
    int64_t state, int64_t options):
  m_ptr(nullptr),
  m_readCb(null_variant),
  m_writeCb(null_variant),
  m_eventCb(null_variant),
  m_cbArg(null_variant)
{
  assert(ctx);
  auto ssl = ::SSL_new(ctx);
  if (!ssl) {
    raise_warning("Failed to create SSL handle");
    return;
  }

#ifndef HPHP_DISABLE_EVENT_PTHREADS
  options |= ::BEV_OPT_THREADSAFE;
#endif
  m_ptr = ::bufferevent_openssl_filter_new(base, bevent, ssl, static_cast<bufferevent_ssl_state>(state), options);

  if (m_ptr == NULL) {
    raise_warning("Failed to allocate bufferevent filter");
    return;
  }
}

EventBufferEvent::EventBufferEvent(struct event_base* base,
    evutil_socket_t fd, SSL_CTX* ctx,
    int64_t state, int64_t options):
  m_ptr(nullptr),
  m_readCb(null_variant),
  m_writeCb(null_variant),
  m_eventCb(null_variant),
  m_cbArg(null_variant)
{
  assert(ctx);
  auto ssl = ::SSL_new(ctx);
  if (!ssl) {
    raise_warning("Failed to create SSL handle");
    return;
  }

  if (fd < 0) {
    raise_warning("Invalid file descriptor");
    return;
  }

  // Attach ectx to ssl for callbacks
  EventBase::m_tlsRequestData->setSslData(ssl, (void *) ctx);

#ifndef HPHP_DISABLE_EVENT_PTHREADS
  options |= ::BEV_OPT_THREADSAFE;
#endif
  m_ptr = ::bufferevent_openssl_socket_new(base, fd, ssl, static_cast<bufferevent_ssl_state>(state), options);
  if (m_ptr == NULL) {
    raise_warning("Failed to allocate bufferevent socket");
    return;
  }
}
#endif // HPHP_DISABLE_EVENT_OPENSSL


void EventBufferEvent::free() {
  if (m_ptr) {
    ::bufferevent_free(m_ptr);
    m_ptr = nullptr;
  }
}


void EventBufferEvent::sweep() {
  free();
}

EventBufferEvent::~EventBufferEvent() {
  free();
}


EventBufferEvent* EventBufferEvent::get(Object obj) {
  return getInternalResource<EventBufferEvent>(obj, s_EventBufferEvent);
}

Object EventBufferEvent::wrap() {
  return newInstance(s_EventBufferEvent.get());
}

Object EventBufferEvent::getInputBuffer() {
  if (m_ptr) {
    return (new EventBuffer(::bufferevent_get_input(m_ptr)))->wrap();
  }
  return null_object;
}

Object EventBufferEvent::getOutputBuffer() {
  if (m_ptr) {
    return (new EventBuffer(::bufferevent_get_output(m_ptr)))->wrap();
  }
  return null_object;
}

/////////////////////////////////////////////////////////////////////////
// EventBufferEvent PHP class

static Variant HHVM_METHOD(EventBufferEvent, _getProperty, int64_t key) {
  HPHP_EVENT_BEVENT_GET(bevent, this_, null_variant);

  switch (key) {
    case 1: // fd
      {
        evutil_socket_t fd = ::bufferevent_getfd(bevent->getPtr());
        return fd == -1 ? null_variant : Variant(fd);
      }
    case 2: // input
      return bevent->getInputBuffer();
    case 3: // output
      return bevent->getOutputBuffer();
    default:
      return null_variant;
  }
}

static void HHVM_METHOD(EventBufferEvent, __constructArray, const Array& args) {
  auto rc = NEWOBJ(EventBufferEvent)(
      args[0].asCObjRef(), // base
      args[1],            // socket
      args[2].toInt64(),  // options
      args[3],            // readcb
      args[4],            // writecb
      args[5],            // writecb
      args[6]);           // arg
  this_->o_set(s__rc, Resource(rc), s_EventBufferEvent.get());
}

static bool HHVM_METHOD(EventBufferEvent, connectHost, const Variant& dnsBaseVar, const String& hostname,
    int64_t port, int64_t family = AF_UNSPEC) {
#if LIBEVENT_VERSION_NUMBER < 0x02000300
  raise_error("Minimum Libevent version required: 2.0.3-alpha");
  return false;
#else

  if (family & ~(AF_INET | AF_INET6 | AF_UNSPEC)) {
    raise_warning("Invalid address family specified");
    return false;
  }

  HPHP_EVENT_BEVENT_GET(bevent, this_, false);

  // bufferevent_socket_connect() allocates a socket stream internally, if we
  // the file descriptor is not provided before e.g. with
  // bufferevent_socket_new()

#ifndef HPHP_DISABLE_EVENT_EXTRA
  EventDnsBase* dnsBase;

  if (dnsBaseVar.isNull()) {
    dnsBase = nullptr;
  } else {
    dnsBase = EventDnsBase::get(dnsBaseVar.toObject());
    if (!dnsBase) {
      raise_warning("EventDnsBase object is not constructed");
      return false;
    }
  }

  if (::bufferevent_socket_connect_hostname(bevent->getPtr(),
        (dnsBase ? dnsBase->getPtr() : NULL),
        family, hostname.c_str(), port)) {
#ifdef HPHP_EVENT_DEBUG
    raise_warning("%s", ::evutil_gai_strerror(
          ::bufferevent_socket_get_dns_error(bevent->getPtr())));
#endif
    return false;
  }

#else //  HPHP_DISABLE_EVENT_EXTRA

  if (::bufferevent_socket_connect_hostname(bevent->getPtr(),
        NULL, family, hostname.c_str(), port)) {
#ifdef PHP_EVENT_DEBUG
    raise_warning("%s", ::evutil_gai_strerror(
          ::bufferevent_socket_get_dns_error(bevent->getPtr())));
#endif
    return false;
  }

#endif  //  HPHP_DISABLE_EVENT_EXTRA

  return true;
#endif //  LIBEVENT_VERSION_NUMBER < 0x02000300
}

static bool HHVM_METHOD(EventBufferEvent, enable, int64_t events) {
  HPHP_EVENT_BEVENT_GET(bevent, this_, false);
  return (::bufferevent_enable(bevent->getPtr(), events) == 0);
}

static bool HHVM_METHOD(EventBufferEvent, disable, int64_t events) {
  HPHP_EVENT_BEVENT_GET(bevent, this_, false);
  return (::bufferevent_disable(bevent->getPtr(), events) == 0);
}

static void HHVM_METHOD(EventBufferEvent, free) {
  HPHP_EVENT_BEVENT_GET(bevent, this_, /* void */);
  //delete bevent;
  bevent->free();
}

static String HHVM_METHOD(EventBufferEvent, getDnsErrorString) {
  HPHP_EVENT_BEVENT_GET(bevent, this_, null_string);

  auto err = ::bufferevent_socket_get_dns_error(bevent->getPtr());
  return err == 0 ? null_string : String(evutil_gai_strerror(err));
}

static int64_t HHVM_METHOD(EventBufferEvent, getEnabled) {
  HPHP_EVENT_BEVENT_GET(bevent, this_, 0);
  return ::bufferevent_get_enabled(bevent->getPtr());
}

static Object HHVM_METHOD(EventBufferEvent, getInput) {
  HPHP_EVENT_BEVENT_GET(bevent, this_, 0);
  return bevent->getInputBuffer();
}

static Object HHVM_METHOD(EventBufferEvent, getOutput) {
  HPHP_EVENT_BEVENT_GET(bevent, this_, 0);
  return bevent->getOutputBuffer();
}

static Variant HHVM_METHOD(EventBufferEvent, read, int64_t size) {
  HPHP_EVENT_BEVENT_GET(bevent, this_, 0);

  char* data = (char *) malloc(size * sizeof(char) + 1);
  auto num = ::bufferevent_read(bevent->getPtr(), data, size);

  if (num > 0) {
    return String(data, num, AttachString);
  }
  return VAR_FALSE;
}

static bool HHVM_METHOD(EventBufferEvent, readBuffer, const Object& bufVar) {
  HPHP_EVENT_BEVENT_GET(bevent, this_, false);
  HPHP_EVENT_BUFFER_GET(buf, bufVar, false);
  return (::bufferevent_read_buffer(bevent->getPtr(), buf->getPtr()) == 0);
}

static void HHVM_METHOD(EventBufferEvent, setCallbacks,
    const Variant& readCbVar, const Variant& writeCbVar, const Variant& eventCbVar, const Variant& arg /*= null_variant*/) {
  HPHP_EVENT_BEVENT_GET(bevent, this_, /* void */);

  bevent->setCallbacks(readCbVar, writeCbVar, eventCbVar, arg);

  bufferevent_data_cb  readCb;
  bufferevent_data_cb  writeCb;
  bufferevent_event_cb eventCb;

  readCb  = f_is_callable(readCbVar)  ? bevent_read_cb  : NULL;
  writeCb = f_is_callable(writeCbVar) ? bevent_write_cb : NULL;
  eventCb = f_is_callable(eventCbVar) ? bevent_event_cb : NULL;

  ::bufferevent_setcb(bevent->getPtr(), readCb, writeCb, eventCb, static_cast<void*>(bevent));
}

static bool HHVM_METHOD(EventBufferEvent, setPriority, int64_t priority) {
  HPHP_EVENT_BEVENT_GET(bevent, this_, false);
  return (::bufferevent_priority_set(bevent->getPtr(), priority) == 0);
}

static bool HHVM_METHOD(EventBufferEvent, setTimeouts,
    double read_timeout, double write_timeout) {
  struct timeval tv_read, tv_write;

  HPHP_EVENT_BEVENT_GET(bevent, this_, false);
  HPHP_EVENT_TIMEVAL_SET(tv_read, read_timeout);
  HPHP_EVENT_TIMEVAL_SET(tv_write, write_timeout);

  return (::bufferevent_set_timeouts(bevent->getPtr(), &tv_read, &tv_write) == 0);
}

static void HHVM_METHOD(EventBufferEvent, setWatermark,
    int64_t events, int64_t lowmark, int64_t highmark) {
  HPHP_EVENT_BEVENT_GET(bevent, this_, /* void */);
  bufferevent_setwatermark(bevent->getPtr(), events,
      static_cast<size_t>(lowmark), static_cast<size_t>(highmark));
}

static Variant HHVM_METHOD(EventBufferEvent, sslError) {
#ifdef HPHP_DISABLE_EVENT_OPENSSL
  HPHP_EVENT_OPENSSL_REQUIRED_ERROR(EventBufferEvent::sslError, null_variant);
#else //  HPHP_DISABLE_EVENT_OPENSSL
  HPHP_EVENT_BEVENT_GET(bevent, this_, VAR_FALSE);

  char buf[512];
  auto e = ::bufferevent_get_openssl_error(bevent->getPtr());
  if (e) {
    return String(ERR_error_string(e, buf));
  }
  return VAR_FALSE;
#endif // HPHP_DISABLE_EVENT_OPENSSL
}

static Object HHVM_STATIC_METHOD(EventBufferEvent, sslFilter, const Object& baseObj,
    const Object& underlyingObj, const Object& ctxObj, int64_t state, int64_t options = 0) {
#ifdef HPHP_DISABLE_EVENT_OPENSSL
  HPHP_EVENT_OPENSSL_REQUIRED_ERROR(EventBufferEvent::sslFilter, null_object);
#else //  HPHP_DISABLE_EVENT_OPENSSL
  if (!EventBufferEvent::isValidSslState(state)) {
    raise_warning("Invalid state specified");
    return null_object;
  }

  HPHP_EVENT_BASE_GET(base, baseObj, null_object);
  HPHP_EVENT_BEVENT_GET(bevUnderlying, underlyingObj, null_object);
  HPHP_EVENT_SSLCONTEXT_GET(ctx, ctxObj, null_object);

  auto rc = NEWOBJ(EventBufferEvent)(base->getPtr(), bevUnderlying->getPtr(),
      ctx->getPtr(), state, options);
  return rc->wrap();
#endif // HPHP_DISABLE_EVENT_OPENSSL
}

static void HHVM_METHOD(EventBufferEvent, sslRenegotiate) {
#ifdef HPHP_DISABLE_EVENT_OPENSSL
  HPHP_EVENT_OPENSSL_REQUIRED_ERROR(EventBufferEvent::sslRenegotiate, /* void */);
#else // HPHP_DISABLE_EVENT_OPENSSL
  HPHP_EVENT_BEVENT_GET(bevent, this_, /* void */);
  ::bufferevent_ssl_renegotiate(bevent->getPtr());
#endif // HPHP_DISABLE_EVENT_OPENSSL
}

static Object HHVM_STATIC_METHOD(EventBufferEvent, sslSocket, const Object& baseObj, const Variant& socketVar,
    const Object& ctxObj, int64_t state, int options = 0) {
#ifdef HPHP_DISABLE_EVENT_OPENSSL
  HPHP_EVENT_OPENSSL_REQUIRED_ERROR(EventBufferEvent::sslSocket, null_object);
#else // HPHP_DISABLE_EVENT_OPENSSL
  evutil_socket_t fd;

  if (!EventBufferEvent::isValidSslState(state)) {
    raise_warning("Invalid state specified");
    return null_object;
  }

  if (socketVar.isNull()) {
    // User decided to set fd later via connect or connectHost etc.
    fd = -1;
  } else {
    fd = Event::varToFd(socketVar);
    if (fd < 0) {
      return null_object;
    }
    // Make sure that the socket is in non-blocking mode(libevent's tip)
    //evutil_make_socket_nonblocking(fd);
  }

  HPHP_EVENT_BASE_GET(base, baseObj, null_object);
  HPHP_EVENT_SSLCONTEXT_GET(ctx, ctxObj, null_object);

  auto rc = NEWOBJ(EventBufferEvent)(base->getPtr(), fd, ctx->getPtr(), state, options);
  return rc->wrap();
#endif // HPHP_DISABLE_EVENT_OPENSSL
}

static bool HHVM_METHOD(EventBufferEvent, write, const String& data) {
  HPHP_EVENT_BEVENT_GET(bevent, this_, false);
  return (::bufferevent_write(bevent->getPtr(), data.data(), data.length()) == 0);
}

static bool HHVM_METHOD(EventBufferEvent, writeBuffer, const Object& bufObj) {
  HPHP_EVENT_BEVENT_GET(bevent, this_, false);
  HPHP_EVENT_BUFFER_GET(buf, bufObj, false);
  return (::bufferevent_write_buffer(bevent->getPtr(), buf->getPtr()) == 0);
}

/////////////////////////////////////////////////////////////////////////

#define REG_NAMED_INT_CONST(v, n) \
  Native::registerClassConstant<KindOfInt64>(s_EventBufferEvent.get(), \
      makeStaticString(#v), n)

#define REG_INT_EVENT_CONST(v) \
  REG_NAMED_INT_CONST(v, BEV_EVENT_ ## v)

#define REG_INT_OPT_CONST(v) \
  REG_NAMED_INT_CONST(OPT_ ## v, BEV_OPT_ ## v)

void EventExtension::_initEventBufferEventClass() {
  HHVM_ME(EventBufferEvent, _getProperty);
  HHVM_ME(EventBufferEvent, __constructArray);
  HHVM_ME(EventBufferEvent, connectHost);
  HHVM_ME(EventBufferEvent, enable);
  HHVM_ME(EventBufferEvent, disable);
  HHVM_ME(EventBufferEvent, free);
  HHVM_ME(EventBufferEvent, getDnsErrorString);
  HHVM_ME(EventBufferEvent, getEnabled);
  HHVM_ME(EventBufferEvent, getInput);
  HHVM_ME(EventBufferEvent, getOutput);
  HHVM_ME(EventBufferEvent, read);
  HHVM_ME(EventBufferEvent, readBuffer);
  HHVM_ME(EventBufferEvent, setCallbacks);
  HHVM_ME(EventBufferEvent, setPriority);
  HHVM_ME(EventBufferEvent, setTimeouts);
  HHVM_ME(EventBufferEvent, setWatermark);
  HHVM_ME(EventBufferEvent, sslError);
  HHVM_STATIC_ME(EventBufferEvent, sslFilter);
  HHVM_ME(EventBufferEvent, sslRenegotiate);
  HHVM_STATIC_ME(EventBufferEvent, sslSocket);
  HHVM_ME(EventBufferEvent, write);
  HHVM_ME(EventBufferEvent, writeBuffer);

  REG_INT_EVENT_CONST(READING);
  REG_INT_EVENT_CONST(WRITING);
  REG_NAMED_INT_CONST(EOF, BEV_EVENT_EOF);
  REG_INT_EVENT_CONST(ERROR);
  REG_INT_EVENT_CONST(TIMEOUT);
  REG_INT_EVENT_CONST(CONNECTED);

  REG_INT_OPT_CONST(CLOSE_ON_FREE);
  REG_INT_OPT_CONST(THREADSAFE);
  REG_INT_OPT_CONST(DEFER_CALLBACKS);
#if LIBEVENT_VERSION_NUMBER >= 0x02000500
  REG_INT_OPT_CONST(UNLOCK_CALLBACKS);
#endif

#ifndef HPHP_DISABLE_EVENT_OPENSSL
  REG_NAMED_INT_CONST(SSL_OPEN,       BUFFEREVENT_SSL_OPEN);
  REG_NAMED_INT_CONST(SSL_CONNECTING, BUFFEREVENT_SSL_CONNECTING);
  REG_NAMED_INT_CONST(SSL_ACCEPTING,  BUFFEREVENT_SSL_ACCEPTING);
#endif
}

#undef REG_NAMED_INT_CONST
#undef REG_INT_EVENT_CONST
#undef REG_INT_OPT_CONST

#undef VAR_FALSE

} // namespace HPHP

// vim: et ts=2 sts=2 sw=2
