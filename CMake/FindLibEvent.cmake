# - Find LibEvent
# This module defines:
# LIBEVENT_INCLUDE_DIR, where to find LibEvent headers
# LIBEVENT_CORE_LIB
# LIBEVENT_EXTRA_LIB
# LIBEVENT_OPENSSL_LIB
# LIBEVENT_PTHREADS_LIB


SET(LibEvent_EXTRA_PREFIXES /usr/local /usr /opt /opt/local "$ENV{HOME}")
FOREACH(prefix ${LibEvent_EXTRA_PREFIXES})
  LIST(APPEND LibEvent_INCLUDE_PATHS "${prefix}/include")
  LIST(APPEND LibEvent_LIB_PATHS "${prefix}/lib")
ENDFOREACH()

FIND_PATH(LIBEVENT_INCLUDE_DIR event2/event.h PATHS ${LibEvent_INCLUDE_PATHS})
FIND_LIBRARY(LIBEVENT_CORE_LIB NAMES event_core PATHS ${LibEvent_LIB_PATHS})

IF (NOT HPHP_DISABLE_EVENT_EXTRA)
  FIND_LIBRARY(LIBEVENT_EXTRA_LIB NAMES event_extra PATHS ${LibEvent_LIB_PATHS})
ENDIF(NOT HPHP_DISABLE_EVENT_EXTRA)

IF (NOT HPHP_DISABLE_EVENT_OPENSSL)
  FIND_LIBRARY(LIBEVENT_OPENSSL_LIB NAMES event_openssl PATHS ${LibEvent_LIB_PATHS})
ENDIF (NOT HPHP_DISABLE_EVENT_OPENSSL)

IF (NOT HPHP_DISABLE_EVENT_PTHREADS)
  FIND_LIBRARY(LIBEVENT_PTHREADS_LIB NAMES event_pthreads PATHS ${LibEvent_LIB_PATHS})
ENDIF (NOT HPHP_DISABLE_EVENT_PTHREADS)

IF (LIBEVENT_CORE_LIB AND LIBEVENT_INCLUDE_DIR)
  MESSAGE(STATUS "Found libevent_core: ${LIBEVENT_CORE_LIB}")
ELSE ()
  MESSAGE(FATAL_ERROR "Could NOT find libevent.")
ENDIF ()

IF (LIBEVENT_EXTRA_LIB)
  MESSAGE(STATUS "Found libevent_extra: ${LIBEVENT_EXTRA_LIB}")
ENDIF (LIBEVENT_EXTRA_LIB)

IF (LIBEVENT_OPENSSL_LIB)
  MESSAGE(STATUS "Found libevent_openssl: ${LIBEVENT_OPENSSL_LIB}")
ENDIF (LIBEVENT_OPENSSL_LIB)

IF (LIBEVENT_PTHREADS_LIB)
  MESSAGE(STATUS "Found libevent_pthreads: ${LIBEVENT_PTHREADS_LIB}")
ENDIF (LIBEVENT_PTHREADS_LIB)


MARK_AS_ADVANCED(
  LIBEVENT_CORE_LIB
  LIBEVENT_EXTRA_LIB
  LIBEVENT_OPENSSL_LIB
  LIBEVENT_PTHREADS_LIB
  LIBEVENT_INCLUDE_DIR
  )
