<?php
function t($message, $expected, $condition) {
  printf("$message %s\n", ($expected == $condition ? 'ok' : 'failed'));
}

function t1($message, $condition) {
  t($message, true, $condition);
}
