<?hh
/*
   +----------------------------------------------------------------------+
   | HipHop for PHP                                                       |
   +----------------------------------------------------------------------+
   | Copyright (c) 2010-2014 Facebook, Inc. (http://www.facebook.com)     |
   | Copyright (c) 1997-2014 The PHP Group                                |
   | Author: Ruslan Osmanov <osmanov@php.net>                             |
   +----------------------------------------------------------------------+
   | This source file is subject to version 3.01 of the PHP license,      |
   | that is bundled with this package in the file LICENSE, and is        |
   | available through the world-wide-web at the following url:           |
   | http://www.php.net/license/3_01.txt                                  |
   | If you did not receive a copy of the PHP license and are unable to   |
   | obtain it through the world-wide-web, please send a note to          |
   | license@php.net so we can mail you a copy immediately.               |
   +----------------------------------------------------------------------+
*/


/**
 * Represents configuration structure which could be used in construction of
 * the EventBase .
 */
final class EventConfig { // {{{
  // Internal resource
  private ?resource $__rc = null;

  /**
   * Constructs EventConfig object
   *
   * @return EventConfig
   */
  <<__Native>>
  public function __construct(): void;

/**
 * Tells libevent to avoid specific event method.
 * See http://www.wangafu.net/~nickm/libevent-book/Ref2_eventbase.html#_creating_an_event_base
 *
 * @param string $method - One of recognized methods (backends):
 * select, poll, epoll, kqueue, devpoll, evport, or win32
 *
 * @return bool - TRUE on success, FALSE otherwise.
 */
  <<__Native>>
  public function avoidMethod(string $method): bool;

/**
 * Enters a required event method feature that the application demands.
 *
 * @param int $features - Bit mask of required features. See `EventConfig::FEATURE_*` constants
 *
 * @return bool - TRUE on success, FALSE otherwise.
 */
  <<__Native>>
  public function requireFeatures(int $features): bool;

  /**
   * Prevents priority inversion by limiting how many low-priority event
   * callbacks can be invoked before checking for more high-priority events.
   *
   * Available since libevent 2.1.0-alpha. For earlier versions does nothing.
   *
   * @param int $max_interval - An interval after which Libevent should stop
   * running callbacks and check for more events, or 0 , if there should be no
   * such interval.
   * @param int $max_callbacks - A number of callbacks after which Libevent
   * should stop running callbacks and check for more events, or -1 , if there
   * should be no such limit.
   * @param int $min_priority - A priority below which $max_interval and
   * $max_callbacks should not be enforced. If this is set to 0 , they are
   * enforced for events of every priority; if it's set to 1 , they're enforced
   * for events of priority 1 and above, and so on.
   *
   * @return void
   */
  <<__Native>>
  public function setMaxDispatchInterval(int $max_interval, int $max_callbacks, int $min_priority): void;
} // class EventConfig }}}


/**
 * EventBase class represents libevent's event base structure. It holds a set
 * of events and can poll to determine which events are active.
 *
 * Each event base has a method, or a backend that it uses to determine which
 * events are ready. The recognized methods are: select , poll , epoll , kqueue
 * , devpoll , evport and win32 .
 *
 * To configure event base to use, or avoid specific backend EventConfig class
 * can be used.
 *
 * WARNING! Do NOT destroy the EventBase object as long as resources of the
 * associated Event objects are not released. Otherwise, it will lead to
 * unpredictable results!
 */
final class EventBase { // {{{
  // Internal resource
  private ?resource $__rc = null;

  <<__Native>>
  public function __construct(?EventConfig $cfg = null): void;

  /**
   * Wait for events to become active, and run their callbacks.
   * The same as EventBase::loop() with no flags set
   *
   * @return bool - TRUE on success, FALSE otherwise.
   */
  <<__Native>>
  public function dispatch(): bool;

  /**
   * Stop dispatching events
   *
   * @param float $timeout - number of seconds after which the event base
   * should stop dispatching events.
   *
   * @return bool - TRUE on success, FALSE otherwise.
   */
  <<__Native>>
  public function exitLoop(float $timeout = -1.0): bool;

  /**
   * Returns bitmask of features supported
   *
   * @return int - bitmask of supported features. See EventConfig::FEATURE_* constants.
   */
  <<__Native>>
  public function getFeatures(): int;

  /**
   * @return string - String representing used event method(backend)
   */
  <<__Native>>
  public function getMethod(): string;

  /**
   * @return float - current time (as returned by gettimeofday()), looking
   * at the cached value in 'base' if possible, and calling gettimeofday() or
   * clock_gettime() as appropriate if there is no cached time; on failure
   * returns 0.0.
   */
  <<__Native>>
  public function getTimeOfDayCached(): float;

  /**
   * @return bool - TRUE if the event loop was told to exit. Otherwise FALSE;
   */
  <<__Native>>
  public function gotExit(): bool;

  /**
   * @return bool - TRUE event loop was told to stop by EventBase::stop(). Otherwise FALSE;
   */
  <<__Native>>
  public function gotStop(): bool;

  /**
   * Dispatch pending events
   *
   * @return bool - TRUE on success, FALSE otherwise.
   */
  <<__Native>>
  public function loop(int $flags = -1): bool;

  /**
   * Sets number of priorities per event base
   *
   * @return bool - TRUE on success, FALSE otherwise.
   */
  <<__Native>>
  public function priorityInit(int $priorities): bool;

  /**
   * Re-initialize event base (after a fork)
   *
   * @return bool - TRUE on success, FALSE otherwise.
   */
  <<__Native>>
  public function reInit(): bool;

  /**
   * Tells event_base to stop dispatching events
   *
   * @return bool - TRUE on success, FALSE otherwise.
   */
  <<__Native>>
  public function stop(): bool;

} // class EventBase }}}


/**
 * Event class represents and event firing on a file descriptor being ready to
 * read from or write to; a file descriptor becoming ready to read from or
 * write to(edge-triggered I/O only); a timeout expiring; a signal occuring; a
 * user-triggered event.
 *
 * Every event is associated with EventBase . However, event will never fire until
 * it is added (via Event::add()). An added event remains in pending state until
 * the registered event occurs, thus turning it to active state. To handle events
 * user may register a callback which is called when event becomes active. If
 * event is configured persistent , it remains pending. If it is not persistent,
 * it stops being pending when it's callback runs. Event::del() method deletes
 * event, thus making it non-pending. By means of Event::add() method it could be
 * added again.
 */
final class Event { // {{{
  // Internal resource
  private ?resource $__rc = null;

  public function __construct(EventBase $base, mixed $fd, int $what, mixed $cb, ?mixed $arg = null): void {
    if ($what & ~Event::ALL_TYPES) {
      trigger_error("Invalid mask", E_ERROR);
      return;
    }
    if (!is_callable($cb)) {
      trigger_error("Function $cb is not callable", E_ERROR);
      return;
    }

    $this->__constructor($base, $fd, $what, $cb, $arg);
  }

  <<__Native>>
  private final function __constructor(EventBase $base, mixed $fd, int $what, mixed $cb, ?mixed $arg = null): void;

  /**
   * Makes event pending
   *
   * Non-pending event will never occur, and the event callback will never be
   * called. In conjuction with Event::del() an event could be re-scheduled by
   * user at any time.
   *
   * If Event::add() is called on an already pending event, Libevent will leave it
   * pending and re-schedule it with the given timeout (if specified). If in this
   * case timeout is not specified, Event::add() has no effect.
   *
   * @param float $timeout = -1.0 - timeout in seconds
   *
   * @return bool - TRUE on success, FALSE otherwise.
   */
  <<__Native>>
   public function add(float $timeout = -1.0): bool;

  /**
   * Makes event non-pending
   */
  <<__Native>>
   public function del(): bool;

  /**
   * Makes event non-pending and free resources allocated for this event.
   */
  <<__Native>>
   public function free(): void;

  /**
   * @return array - names of the methods supported in this version of Libevent
   */
  <<__Native>>
   public static function getSupportedMethods(): array;

  /**
   * Detects whether event is pending or scheduled.
   *
   * @param int $flags - One of, or a composition of the following constants:
   * Event::READ, Event::WRITE, Event::TIMEOUT, Event::SIGNAL .
   *
   * @return bool - TRUE if event is pending or scheduled; otherwise FALSE.
   */
  <<__Native>>
   public function pending(int $flags): bool;

  /**
   * Re-configures event
   *
   * @param EventBase $base
   * @param mixed $fd - file descriptor or socket resource
   * @param int $what = -1 - Event flags
   * @param mixed $cb = null - callback
   * @param mixed $arg = null - argument for callback $cb
   *
   * @return bool - TRUE on success, FALSE otherwise.
   */
  <<__Native>>
    public function set(EventBase $base, mixed $fd, int $what = -1,
      ?mixed $cb = null, ?mixed $arg = null): bool;

  /**
   * Set event priority
   *
   * @return bool - TRUE on success, FALSE otherwise.
   */
  <<__Native>>
   public function setPriority(int $priority): bool;

  /**
   * Re-configures timer event.
   *
   * Note, this function doesn't invoke obsolete libevent's event_set. It calls event_assign instead.
   *
   * @param EventBase $base
   * @param mixed $cb - callback
   * @param mixed $arg = null - argument for callback $cb
   *
   * @return bool - TRUE on success, FALSE otherwise.
   */
  <<__Native>>
   public function setTimer(EventBase $base, mixed $cb, ?mixed $arg = null): bool;

  /**
   * Constructs signal event object
   *
   * @param EventBase $base
   * @param int $signum - signal number
   * @param mixed $cb - callback
   * @param mixed $arg = null - argument for callback $cb
   *
   * @return Event
   */
  <<__Native>>
   public static function signal(EventBase $base, int $signum, mixed $cb, ?mixed $arg = null): Event;

  /**
   * Constructs timer event object
   *
   * @param EventBase $base
   * @param mixed $cb - callback
   * @param mixed $arg = null - argument for callback $cb
   *
   * @return Event
   */
  <<__Native>>
   public static function timer(EventBase $base, mixed $cb, ?mixed $arg = null): Event;

} // class Event }}}


/**
 * EventBuffer represents Libevent's "evbuffer", an utility functionality for
 * buffered I/O.
 *
 * Event buffers are meant to be generally useful for doing the "buffer" part
 * of buffered network I/O.
 */
final class EventBuffer { // {{{
  // Internal resource
  private ?resource $__rc = null;

  /**
   * @return EventBuffer
   */
  <<__Native>>
  public function __construct(): void;

  /**
   * Append data to the end of an event buffer
   *
   * @param string $s - String to be appended to the end of the buffer
   *
   * @return bool - TRUE on success, FALSE otherwise.
   */
  <<__Native>>
  public function add(string $s): bool;

  /**
   * Move all data from a buffer provided to the current instance of EventBuffer
   *
   * @param EventBuffer $src - source object
   *
   * @return bool - TRUE on success, FALSE otherwise.
   */
  <<__Native>>
  public function addBuffer(EventBuffer $src): bool;

  /**
   * @return int - The number of bytes stored in the buffer
   */
  <<__Native>>
  public function getLength(): int;

  /**
   * @return int - The number of bytes stored contiguously at the front of the
   * buffer. The bytes in a buffer may be stored in multiple separate chunks of
   * memory; the property returns the number of bytes currently stored in the
   * first chunk.
   */
  <<__Native>>
  public function getContiguousSpace(): int;

  /**
   * Moves the specified number of bytes from a source buffer to the end of the
   * current buffer. If there are fewer number of bytes, it moves all the bytes
   * available from the source buffer.
   *
   * @return Returns the number of bytes read.
   */
  <<__Native>>
  public function appendFrom(EventBuffer $buf, int $len): int;

  /**
   * Just like EventBuffer::read(), except it does not drain any data from
   * the buffer. I.e. it copies the first $max_bytes bytes from the front of the
   * buffer into data. If there are fewer than $max_bytes bytes available, the
   * function copies all the bytes there are.
   *
   * @param mixed $s - Output string.
   * @param int $max_bytes - The number of bytes to copy.
   *
   * @return Returns the number of bytes copied, or -1 on failure.
   */
  <<__Native>>
  public function copyout(mixed &$s, int $max_bytes): int;

  /**
   * Removes specified number of bytes from the front of the buffer without
   * copying it anywhere.
   *
   * @param int $len - The number of bytes to remove from the buffer.
   *
   * @return bool - TRUE on success, FALSE otherwise.
   */
  <<__Native>>
  public function drain(int $len): bool;

  /**
   * Enable locking on an EventBuffer so that it can safely be used by multiple
   * threads at the same time. When locking is enabled, the lock will be held
   * when callbacks are invoked. This could result in deadlock if you aren't
   * careful. Plan accordingly!
   */
  <<__Native>>
  public function enableLocking(): void;

  /**
   * Reserves space in buffer.
   *
   * Alters the last chunk of memory in the buffer, or adds a new chunk, such
   * that the buffer is now large enough to contain len bytes without any
   * further allocations.
   *
   * @return bool - TRUE on success, FALSE otherwise.
   */
  <<__Native>>
  public function expand(int $len): bool;

  /**
   * Prevent calls that modify an event buffer from succeeding
   *
   * @return bool - TRUE on success, FALSE otherwise.
   */
  <<__Native>>
  public function freeze(bool $at_front): bool;

  /**
   * Acquires a lock on buffer. Can be used in pair with EventBuffer::unlock()
   * to make a set of operations atomic, i.e. thread-safe. Note, it is not
   * needed to lock buffers for individual operations. When locking is
   * enabled(see EventBuffer::enableLocking() ), individual operations on event
   * buffers are already atomic.
   *
   * @return void
   */
  <<__Native>>
  public function lock(): void;

  /**
   * Releases lock acquired by EventBuffer::lock
   *
   * @return void
   */
  <<__Native>>
  public function unlock(): void;

  /**
   * Prepend data to the front of the buffer.
   *
   * @param string $data - String to be prepended to the front of the buffer.
   *
   * @return bool - TRUE on success, FALSE otherwise.
   */
  <<__Native>>
  public function prepend(string $data): bool;

  /**
   * Moves all data from source buffer to the front of current buffer.
   *
   * @param EventBuffer $src - Source buffer.
   *
   * @return bool - TRUE on success, FALSE otherwise.
   */
  <<__Native>>
  public function prependBuffer(EventBuffer $src): bool;

  /**
   * "Linearizes" the first size bytes of the buffer, copying or moving them as
   * needed to ensure that they are all contiguous and occupying the same chunk
   * of memory. If size is negative, the function linearizes the entire buffer.
   *
   * WARNING! Calling EventBuffer::pullup() with a large size can be quite
   * slow, since it potentially needs to copy the entire buffer's contents.
   *
   * @param int $size - The number of bytes required to be contiguous within the buffer
   *
   * @return If $size is greater than the number of bytes in the buffer, the
   * function returns NULL. Otherwise, returns string.
   */
  <<__Native>>
  public function pullup(int $size): mixed;

  /**
   * Read data from an evbuffer and drain the bytes read
   *
   * @param int $max_bytes - Maxmimum number of bytes to read from the buffer.
   *
   * @return string - Returns string read, or FALSE on failure
   */
  <<__Native>>
  public function read(int $max_bytes): mixed;

  /**
   * Read data from the file specified by $fd onto the end of the buffer.
   *
   * @param mixed $fd - Socket resource or numeric file descriptor
   * @param int $howmuch - Maxmimum number of bytes to read.
   *
   * @return int - Returns the number of bytes read, or FALSE on failure
   */
  <<__Native>>
  public function readFrom(mixed $fd, int $howmuch): mixed;

  /**
   * Extracts a line from the front of the buffer and returns it in a newly
   * allocated string. If there is not a whole line to read, the function
   * returns NULL. The line terminator is not included in the copied string.
   *
   * @param int $eol_style - One of EventBuffer:EOL_* constants
   *
   * @return string - On success returns the line read from the buffer, otherwise NULL
   */
  <<__Native>>
  public function readLine(int $eol_style): mixed;

  /**
   * Scans the buffer for an occurrence of the string what . It returns numeric
   * position of the string, or FALSE if the string was not found.
   *
   * If the start argument is provided, it points to the position at which the
   * search should begin; otherwise, the search is performed from the start of
   * the string. If end argument provided, the search is performed between
   * start and end buffer positions.
   *
   * @param string $what - string to search
   * @param int $start = -1 - start position
   * @param int $end = -1 - end position
   *
   * @return int - Returns numeric position of the first occurance of the
   * string in the buffer, or FALSE if string is not found.
   */
  <<__Native>>
  public function search(string $what, int $start = -1, int $end = -1): mixed;

  /**
   * Scans the buffer for an occurrence of an end of line specified by
   * eol_style parameter . It returns numeric position of the string, or FALSE
   * if the string was not found.
   *
   * If the start argument is provided, it represents the position at which the
   * search should begin; otherwise, the search is performed from the start of
   * the string. If end argument provided, the search is performed between
   * start and end buffer positions.
   *
   * @param int $start = -1 - start position
   * @param int $eol_style = EventBuffer::EOL_ANY - one of EventBuffer::EOL_*
   * constants
   *
   * @return int -  numeric position of the first occurance of end-of-line
   * symbol in the buffer, or FALSE if not found.
   */
  <<__Native>>
  public function searchEol(int $start = -1, int $eol_style = EventBuffer::EOL_ANY): mixed;

  /**
   * Substracts up to $length bytes of the buffer data beginning at $start position.
   *
   * @param int $start - start position
   * @param int $length = -1 - Maximum number of bytes to substract
   *
   * @return string - the data substracted as a string on success, or FALSE on failure.
   */
  <<__Native>>
  public function substr(int $start, int $length = -1): mixed;

  /**
   * Re-enable calls that modify an event buffer.
   *
   * @param bool $at_front - whether to enable events at the front or at the end of the buffer.
   *
   * @return bool - TRUE on success, FALSE otherwise.
   */
  <<__Native>>
  public function unfreeze(bool $at_front): bool;

  /**
   * Write contents of the buffer to a file or socket
   *
   * @param mixed $fd - socket resource or numeric file descriptor
   * @param int $howmuch = -1 - The maximum number of bytes to write.
   *
   * @return int - the number of bytes written, or FALSE on error.
   */
  <<__Native>>
  public function write(mixed $fd, int $howmuch = -1): mixed;
} // class EventBuffer }}}

class EventUtil { // {{{
  <<__Native>>
    public static function getLastSocketErrno(?mixed $socket = null): int;
} // }}}

final class EventSslContext { // {{{
  // Internal resource
  private ?resource $__rc = null;
} // }}}

/**
 * Represents Libevent's buffer event.
 *
 * Usually an application wants to perform some amount of data buffering in
 * addition to just responding to events. When we want to write data, for
 * example, the usual pattern looks like:
 *
 * - Decide that we want to write some data to a connection; put that data in a
 * buffer.
 * - Wait for the connection to become writable
 * - Write as much of the data as we can
 * - Remember how much we wrote, and if we still have more data to write, wait
 * for the connection to become writable again.
 *
 * This buffered I/O pattern is common enough that Libevent provides a generic
 * mechanism for it. A "buffer event" consists of an underlying transport (like
 * a socket), a read buffer, and a write buffer. Instead of regular events,
 * which give callbacks when the underlying transport is ready to be read or
 * written, a buffer event invokes its user-supplied callbacks when it has read
 * or written enough data.
 */
final class EventBufferEvent { //{{{
  // Internal resource
  private ?resource $__rc = null;

  public function __get($name) {
    switch ($name) {
    case 'fd':
      return $this->_getProperty(1);
    case 'input':
      return $this->_getProperty(2);
    case 'output':
      return $this->_getProperty(3);
    }
    trigger_error("Undefined property " . __CLASS__ . "::$name", E_USER_WARNING);
  }

  /**
   * Helper for getting property values
   *
   * @param int $property - internal property key
   * @return mixed
   */
  <<__Native>>
  private function _getProperty(int $property): mixed;

  /**
   * Create a buffer event on a socket, stream or a file descriptor. Passing
   * NULL to socket means that the socket should be created later, e.g. by
   * means of EventBufferEvent::connect().
   *
   * @param EventBase $base - Event base that should be associated with the new buffer event.
   * @param mixed $socket = null - Socket stream or numeric file descriptor
   * @param int $options = 0 - One of EventBufferEvent::OPT_* constants , or 0.
   * @param mixed $readcb = null - Read event callback. See About buffer event callbacks.
   * @param mixed $writecb = null - Write event callback. See About buffer event callbacks.
   * @param mixed $eventcb = null - Status-change event callback. See About buffer event callbacks.
   * @param mixed $arg = null - A variable that will be passed to all the callbacks.
   *
   * @return EventBufferEvent
   */
  public final function __construct(EventBase $base, ?mixed $socket = null, int $options = 0,
    ?mixed $readcb = null, ?mixed $writecb = null, ?mixed $eventcb = null, ?mixed $arg = null
  ): void {
    $this->__constructArray([$base, $socket, $options, $readcb, $writecb, $eventcb, $arg]);
  }

  /**
   * Native methods are limited to 5 arguments.
   * This method is a workaround.
   */
  <<__Native>>
  private function __constructArray(array $arg): void;

  /**
   * Resolves the DNS name hostname, looking for addresses of type family (
   * EventUtil::AF_* constants). If the name resolution fails, it invokes the
   * event callback with an error event. If it succeeds, it launches a
   * connection attempt just as EventBufferEvent::connect() would.
   *
   * @param EventDnsBase $dns_base - Optional. May be NULL. For asyncronous
   * hostname resolving pass a valid event dns base resource. Otherwise the
   * hostname resolving will block.
   *
   * @param string $hostname - Hostname to connect to. Recognized formats are:
   * <pre>
   * www.example.com (hostname)
   * 1.2.3.4 (ipv4address)
   * ::1 (ipv6address)
   * [::1] ([ipv6address])
   * </pre>
   *
   * @param int $port - port number
   * @param int $family = EventUtil::AF_UNSPEC - One of EventUtil::AF_* constants.
   */
  <<__Native>>
  public function connectHost(mixed $dns_base, string $hostname, int $port,
    int $family = EventUtil::AF_UNSPEC): bool;

  /**
   * Changes one or more of the callbacks of a buffer event.
   * A callback may be disabled by passing NULL instead of the callable.
   *
   * @param mixed $readcb - read callback
   * @param mixed $writecb - write callback
   * @param mixed $eventcb  - event callback
   * @param mixed $arg - user data passed to the callbacks.
   *
   * @return void
   */
  <<__Native>>
  public function setCallbacks(mixed $readcb, mixed $writecb, mixed $eventcb,
    ?mixed $arg = null): void;

  /**
   * Enable events Event::READ, Event::WRITE, or Event::READ | Event::WRITE on
   * a buffer event.
   *
   * @param int $events - Event::READ , Event::WRITE , or Event::READ | Event::WRITE
   *
   * @return bool - TRUE on success, FALSE otherwise.
   */
  <<__Native>>
  public function enable(int $events): bool;

  /**
   * Disable events Event::READ, Event::WRITE, or Event::READ | Event::WRITE on
   * a buffer event.
   *
   * @param int $events - Event::READ , Event::WRITE , or Event::READ | Event::WRITE
   *
   * @return bool - TRUE on success, FALSE otherwise.
   */
  <<__Native>>
  public function disable(int $events): bool;

  /**
   * Free resources allocated by buffer event.
   *
   * Usually there is no need to call this method, since normally it is done
   * within internal object destructors. However, sometimes we have a long-time
   * script allocating lots of instances, or a script with a heavy memory
   * usage, where we need to free resources as soon as possible. In such cases
   * EventBufferEvent::free() may be used to protect the script against running
   * up to the `memory_limit`.
   *
   * @return void
   */
  <<__Native>>
  public function free(): void;

  /**
   * Returns string describing the last failed DNS lookup attempt made by
   * EventBufferEvent::connectHost() , or an empty string, if there is no DNS
   * error detected.
   *
   * @return string
   */
  <<__Native>>
  public function getDnsErrorString(): string;

  /**
   * Returns bitmask of events currently enabled on the buffer event
   *
   * @return int - a bitmask of events currently enabled on the buffer event
   */
  <<__Native>>
  public function getEnabled(): int;

  /**
   * Returns underlying input buffer associated with current buffer event. An
   * input buffer is a storage for data to read.
   *
   * @note There is also `input` property.
   *
   * @return EventBuffer - input buffer associated with current buffer event
   */
  <<__Native>>
  public function getInput(): object;

  /**
   * Returns underlying output buffer associated with current buffer event. An
   * output buffer is a storage for data to read.
   *
   * @note There is also `output` property.
   *
   * @return EventBuffer - output buffer associated with current buffer event
   */
  <<__Native>>
  public function getOutput(): object;

  /**
   * Reads (and removes) up to $size bytes from the input buffer.
   *
   * @param $size - Maximum number of bytes to read.
   *
   * @return mixed - string data read from the input buffer on success, or
   * FALSE in case of failure.
   */
  <<__Native>>
  public function read(int $size): mixed;

  /**
   * Drains the entire contents of the input buffer and places them into $buf.
   *
   * @param EventBuffer $buf - target buffer
   *
   * @return bool - TRUE on success, FALSE otherwise.
   */
  <<__Native>>
  public function readBuffer(EventBuffer $buf): bool;

  /**
   * Assign a priority to a bufferevent
   *
   * @param int $priority - Priority value.
   *
   * @return bool - TRUE on success, FALSE otherwise.
   */
  <<__Native>>
  public function setPriority(int $priority): bool;

  /**
   * Set the read and write timeout for a buffer event
   *
   * @param float $read_timeout
   * @param float $write_timeout
   *
   * @return bool - TRUE on success, FALSE otherwise.
   */
  <<__Native>>
  public function setTimeouts(float $read_timeout, float $write_timeout): bool;

  /**
   * Adjusts the read watermarks, the write watermarks , or both, of a single
   * buffer event.
   *
   * A buffer event watermark is an edge, a value specifying number of bytes to
   * be read or written before callback is invoked. By default every read/write
   * event triggers a callback invokation.
   *
   * @see @link http://www.wangafu.net/~nickm/libevent-book/Ref6_bufferevent.html#_callbacks_and_watermarks
   *
   * @param int $events - Bitmask of Event::READ , Event::WRITE , or both.
   * @param int $highmark - Minimum watermark value.
   * @param int $lowmark - Maximum watermark value. 0 means "unlimited".
   *
   * @return void
   */
  <<__Native>>
  public function setWatermark(int $events, int $highmark, int $lowmark): void;

  /**
   * Returns most recent OpenSSL error reported on the buffer event
   *
   * @return mixed - OpenSSL error string, or FALSE if there is no more error
   * to return.
   */
  <<__Native>>
  public function sslError(): mixed;

  /**
   * Create a new SSL buffer event to send its data over another buffer event
   *
   * @param EventBase $base
   * @param EventBufferEvent $underlying - A socket buffer event to use for this SSL.
   * @param EventSslContext $ctx
   * @param int $state - The current state of SSL connection:
   * EventBufferEvent::SSL_OPEN, EventBufferEvent::SSL_ACCEPTING or
   * EventBufferEvent::SSL_CONNECTING .
   * @param int $options = 0 - One or more buffer event options.
   *
   * @return EventBufferEvent
   */
  <<__Native>>
  public static function sslFilter(EventBase $base, EventBufferEvent $underlying,
    EventSslContext $ctx, int $state, int $options = 0): object;

  /**
   * Tells a bufferevent to begin SSL renegotiation.
   *
   * @warning Calling this function tells the SSL to renegotiate, and the
   * buffer event to invoke appropriate callbacks. This is an advanced topic;
   * this should be generally avoided unless one really knows what he/she does,
   * especially since many SSL versions have had known security issues related
   * to renegotiation.
   *
   * @return void
   */
  <<__Native>>
  public function sslRenegotiate(): void;

  /**
   * @param EventBase $base
   *
   * @param mixed $socket - Socket to use for this SSL. Can be stream or socket
   * resource, numeric file descriptor, or NULL. If socket is NULL, it is
   * assumed that the file descriptor for the socket will be assigned later,
   * for instance, by means of EventBufferEvent::connectHost() method.
   *
   * @param EventSslContext $ctx
   *
   * @param int $state - The current state of SSL connection:
   * EventBufferEvent::SSL_OPEN, EventBufferEvent::SSL_ACCEPTING or
   * EventBufferEvent::SSL_CONNECTING .
   *
   * @param int $options = 0 - One or more buffer event options.
   *
   * @return EventBufferEvent
   */
  <<__Native>>
  public static function sslSocket(EventBase $base, ?mixed $socket,
    EventSslContext $ctx, int $state, int $options = 0): object;

  /**
   * Adds data to a buffer event's output buffer
   *
   * @param string $data - Data to be added to the underlying buffer.
   *
   * @return bool - TRUE on success, FALSE otherwise.
   */
  <<__Native>>
  public function write(string $data): bool;

  /**
   * Adds contents of the entire buffer to a buffer event's output buffer
   *
   * @param EventBuffer $buf - Source EventBuffer object.
   *
   * @return bool - TRUE on success, FALSE otherwise.
   */
  <<__Native>>
  public function writeBuffer(EventBuffer $buf): bool;
} //}}}

final class EventDnsBase { // {{{
  // Internal resource
  private ?resource $__rc = null;

  /**
   * Constructs EventDnsBase object
   *
   * @param EventBase $base
   * @param bool $initialize - If the initialize argument is TRUE, it tries to
   * configure the DNS base sensibly given your operating system’s default.
   * Otherwise, it leaves the event DNS base empty, with no nameservers or
   * options configured. In the latter case DNS base should be configured
   * manually, e.g. with EventDnsBase::parseResolvConf()
   *
   * @return EventDnsBase
   */
  <<__Native>>
  public final function __construct(EventBase $base, bool $initialize): void;

  /**
   * Adds a nameserver to the DNS base
   *
   * @param string $ip - The nameserver string, either as an IPv4 address, an
   * IPv6 address, an IPv4 address with a port ( IPv4:Port ), or an IPv6
   * address with a port ( [IPv6]:Port ).
   *
   * @return bool - TRUE on success, FALSE otherwise.
   */
  <<__Native>>
  public function addNameserverIp(string $ip): bool;

  /**
   * Adds a domain to the list of search domains
   *
   * @param string $domain - search domain
   *
   * @return void
   */
  <<__Native>>
  public function addSearch(string $domain): void;

  /**
   * Removes all current search suffixes from the DNS base; the
   * EventDnsBase::addSearch() function adds a suffix.
   *
   * @return void
   */
  <<__Native>>
  public function clearSearch(): void;

  /**
   * Gets the number of configured nameservers
   *
   * @return  the number of configured nameservers(not necessarily the number
   * of running nameservers). This is useful for double-checking whether our
   * calls to the various nameserver configuration functions have been
   * successful.
   */
  <<__Native>>
  public function countNameservers(): int;

  /**
   * Loads a hosts file (in the same format as /etc/hosts ) from hosts file.
   *
   * @param string $hosts - Path to the hosts' file
   *
   * @return bool - TRUE on success, FALSE otherwise.
   */
  <<__Native>>
  public function loadHosts(string $hosts): bool;

  /**
   * Scans the resolv.conf-formatted file stored in filename, and read in all
   * the options from it that are listed in flags
   *
   * @param int $flags - Determines what information is parsed from the
   * resolv.conf file. Any of EventDnsBase::OPTION_* constants.
   * See the man page for resolv.conf for the format of this
   * file.
   * The following directives are not parsed from the file: sortlist, rotate,
   * no-check-names, inet6, debug .
   * If this function encounters an error, the possible return values are:
   * 1 = failed to open file
   * 2 = failed to stat file
   * 3 = file too large
   * 4 = out of memory
   * 5 = short read from file
   * 6 = no nameservers listed in the file
   *
   * @param string $filename
   *
   * @return bool - TRUE on success, FALSE otherwise.
   */
  <<__Native>>
  public function parseResolvConf(int $flags, string $filename): bool;

  /**
   * Set the value of a configuration option.
   *
   * @param string $option -  configuration options are: "ndots", "timeout",
   * "max-timeouts", "max-inflight", and "attempts".
   *
   * @param string $value - option value
   *
   * @return bool - TRUE on success, FALSE otherwise.
   */
  <<__Native>>
  public function setOption(string $option, string $value): bool;

  /**
   * Set the 'ndots' parameter for searches. Sets the number of dots which,
   * when found in a name, causes the first query to be without any search
   * domain.
   *
   * @param int $ndots - number of dots
   *
   * @return void
   */
  <<__Native>>
  public function setSearchNdots(int $ndots): void;
} // }}}

// vim: fdm=marker
