# HHVM Event extension

This is a port of PECL Event extension.

*This extension is under development. Pull requests, patches, and other kinds of contribution are welcome.* 

## API differences from the PECL extension

* EventBase::exit renamed to EventBase::exitLoop because HHVM denies "exit" method
* EventBase::getTimeOfDayCached returns 0.0 on failure (not NULL)
* `Event::ALL_TYPES` constant added
* The mixed file descriptor argument now accepts only numeric file descriptors and socket resources.
* `Event::addSignal` (alias of `Event::add`) removed
* `EventBuffer` `length` and `contiguous_space` properties are replaced with `getLength` and `getContiguousSpace` methods (maybe rather add getter as in `EventBufferEvent`?)
* `EventBufferEvent` callbacks now don't accept `EventBufferEvent` object as the first argument

# TODO

- Some tests are failing with JUT turned on.
- Request init/shutdown handlers
- SSL

# Building HHVM

Note, if HHVM build fails on link stage complaining about not found `mysqlclient_r`,
then add the following to *HHVM*'s `CMake/FindMySQL.cmake`:

```cmake
FIND_PATH(MYSQL_INCLUDE_DIR mysql.h
    /usr/include/mysql
    NO_DEFAULT_PATH)
...



FIND_LIBRARY(MYSQL_LIB NAMES mysqlclient_r
    PATHS
    /usr/lib/mysql
    NO_DEFAULT_PATH)

OR rather

FIND_LIBRARY(MYSQL_LIB NAMES mysqlclient_r
    PATHS
    /usr/lib64/mysql
    NO_DEFAULT_PATH)

for 64-bit arch. See https://github.com/facebook/hhvm/issues/1531
```


Building with debug support:
```
./configure -DCMAKE_BUILD_TYPE=Debug
make -j7 # <-- edit this
```

# Author

Ruslan Osmanov <osmanov@php.net>
