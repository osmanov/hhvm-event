/*
   +----------------------------------------------------------------------+
   | HipHop for PHP                                                       |
   +----------------------------------------------------------------------+
   | Copyright (c) 2010-2014 Facebook, Inc. (http://www.facebook.com)     |
   | Copyright (c) 1997-2014 The PHP Group                                |
   | Author: Ruslan Osmanov <osmanov@php.net>                             |
   +----------------------------------------------------------------------+
   | This source file is subject to version 3.01 of the PHP license,      |
   | that is bundled with this package in the file LICENSE, and is        |
   | available through the world-wide-web at the following url:           |
   | http://www.php.net/license/3_01.txt                                  |
   | If you did not receive a copy of the PHP license and are unable to   |
   | obtain it through the world-wide-web, please send a note to          |
   | license@php.net so we can mail you a copy immediately.               |
   +----------------------------------------------------------------------+
*/

#include "ext_event.h"

namespace HPHP {




const StaticString s__rc("__rc");
const StaticString s_EventConfig("EventConfig");
const StaticString s_EventBase("EventBase");
const StaticString s_Event("Event");
const StaticString s_EventBuffer("EventBuffer");
const StaticString s_EventBufferEvent("EventBufferEvent");
const StaticString s_EventDnsBase("EventDnsBase");
const StaticString s_EventUtil("EventUtil");
const StaticString s_EventSslContext("EventSslContext");

EventExtension s_event_extension;


Object EventResourceData::newInstance(const String& ctx) {
  auto cls = Unit::lookupClass(ctx.get());
  assert(cls);
  auto obj = ObjectData::newInstance(cls);
  assert(obj);
  Object ret(obj);
  obj->o_set(s__rc, Resource(this), ctx);
  return ret;
}




HHVM_GET_MODULE(event);

} // namespace HPHP

// vim: et ts=2 sts=2 sw=2
