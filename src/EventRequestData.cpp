/*
   +----------------------------------------------------------------------+
   | HipHop for PHP                                                       |
   +----------------------------------------------------------------------+
   | Copyright (c) 2010-2014 Facebook, Inc. (http://www.facebook.com)     |
   | Copyright (c) 1997-2014 The PHP Group                                |
   | Author: Ruslan Osmanov <osmanov@php.net>                             |
   +----------------------------------------------------------------------+
   | This source file is subject to version 3.01 of the PHP license,      |
   | that is bundled with this package in the file LICENSE, and is        |
   | available through the world-wide-web at the following url:           |
   | http://www.php.net/license/3_01.txt                                  |
   | If you did not receive a copy of the PHP license and are unable to   |
   | obtain it through the world-wide-web, please send a note to          |
   | license@php.net so we can mail you a copy immediately.               |
   +----------------------------------------------------------------------+
*/

#include "ext_event.h"
#include "src/EventRequestData.h"

namespace HPHP {

#if LIBEVENT_VERSION_NUMBER < 0x02001900
# define HPHP_EVENT_LOG_CONST(name) _ ## name
#else
# define HPHP_EVENT_LOG_CONST(name) name
#endif

// Overrides libevent's default error logging(it logs to stderr)
static void log_cb(int severity, const char* msg) {
  ErrorConstants::ErrorModes mode;

  switch (severity) {
    case HPHP_EVENT_LOG_CONST(EVENT_LOG_DEBUG):
      mode = ErrorConstants::ErrorModes::STRICT;
    case HPHP_EVENT_LOG_CONST(EVENT_LOG_MSG):
      mode = ErrorConstants::ErrorModes::NOTICE;
    case HPHP_EVENT_LOG_CONST(EVENT_LOG_WARN):
      mode = ErrorConstants::ErrorModes::WARNING;
    case HPHP_EVENT_LOG_CONST(EVENT_LOG_ERR):
      mode = ErrorConstants::ErrorModes::ERROR;
    default:
      mode = ErrorConstants::ErrorModes::NOTICE;
  }
  std::string smsg = std::string(msg);
  raise_message(mode, smsg);
}


// Is called when Libevent detects a non-recoverable internal error.
static void fatal_error_cb(int err) {
  raise_error("libevent detected a non-recoverable internal error, "
      "code: %d", err);
}


#ifndef HPHP_DISABLE_EVENT_OPENSSL
void EventRequestData::setSslData(SSL* ssl, void* ptr) {
  ::SSL_set_ex_data(ssl, m_sslDataIndex, ptr);
}
#endif


void EventRequestData::requestInit() {
#ifdef HPHP_EVENT_DEBUG
  ::event_enable_debug_mode();
#endif
#ifndef HPHP_DISABLE_EVENT_PTHREADS
# ifndef EVTHREAD_USE_PTHREADS_IMPLEMENTED
#   error Libevent was built without support for pthreads
# endif
  if (::evthread_use_pthreads()) {
    raise_error("evthread_use_pthreads() failed");
  }
#endif
  // Handle libevent's error logging more gracefully than it's default
  // logging to stderr, or calling abort()/exit()
  ::event_set_fatal_callback(fatal_error_cb);
  ::event_set_log_callback(log_cb);

#ifndef HPHP_DISABLE_EVENT_OPENSSL
  // Initialize openssl library
  ::SSL_library_init();
  ::OpenSSL_add_all_ciphers();
  ::OpenSSL_add_all_digests();
  ::OpenSSL_add_all_algorithms();
  ::SSL_load_error_strings();

  // Create new index which will be used to retreive custom data of the OpenSSL callbacks
  m_sslDataIndex = ::SSL_get_ex_new_index(0, (void *) "PHP EventSslContext index", NULL, NULL, NULL);
#endif // HPHP_DISABLE_EVENT_OPENSSL
}

void EventRequestData::requestShutdown() {
#ifndef HPHP_DISABLE_EVENT_OPENSSL
  // Removes memory allocated when loading digest and cipher names
  // in the OpenSSL_add_all_ family of functions
  ::EVP_cleanup();
#endif

#if LIBEVENT_VERSION_NUMBER >= 0x02010000
  // libevent_global_shutdown is available since libevent 2.1.0-alpha.
  //
  // Make sure that libevent has released all internal library-global data
  // structures. Don't call any of libevent functions below!
  ::libevent_global_shutdown();
#endif
}

} // namespace HPHP

// vim: et ts=2 sts=2 sw=2
