/*
   +----------------------------------------------------------------------+
   | HipHop for PHP                                                       |
   +----------------------------------------------------------------------+
   | Copyright (c) 2010-2014 Facebook, Inc. (http://www.facebook.com)     |
   | Copyright (c) 1997-2014 The PHP Group                                |
   | Author: Ruslan Osmanov <osmanov@php.net>                             |
   +----------------------------------------------------------------------+
   | This source file is subject to version 3.01 of the PHP license,      |
   | that is bundled with this package in the file LICENSE, and is        |
   | available through the world-wide-web at the following url:           |
   | http://www.php.net/license/3_01.txt                                  |
   | If you did not receive a copy of the PHP license and are unable to   |
   | obtain it through the world-wide-web, please send a note to          |
   | license@php.net so we can mail you a copy immediately.               |
   +----------------------------------------------------------------------+
*/

#ifndef incl_HPHP_EXT_EVENT_H
#define incl_HPHP_EXT_EVENT_H

#include "hphp/runtime/base/base-includes.h"
#include "hphp/runtime/vm/native-data.h"
#include "hphp/runtime/base/socket.h"
#include "hphp/runtime/vm/jit/translator-inline.h" // JIT::VMRegAnchor

#include <sys/un.h>
#include <signal.h>

#if 0
#ifdef PHP_EVENT_SOCKETS
# include "ext/sockets/php_sockets.h"
# define PHP_EVENT_SOCKETS_SUPPORT
#endif
#endif

#include <event2/event.h>
#include <event2/bufferevent.h>
#include <event2/buffer.h>
#include <event2/util.h>

#ifndef HPHP_DISABLE_EVENT_PTHREADS
# include <event2/thread.h>
#endif

#ifndef HPHP_DISABLE_EVENT_EXTRA
# include <event2/keyvalq_struct.h>
# include <event2/listener.h>
# include <event2/dns.h>
# include <event2/http.h>
# include <event2/rpc.h>
# include <event2/tag.h>
#endif

#ifndef HPHP_DISABLE_EVENT_OPENSSL
# include <event2/bufferevent_ssl.h>
#endif

#if !defined(_MINIX)
# include <pthread.h>
#endif

#if !defined(LIBEVENT_VERSION_NUMBER) || LIBEVENT_VERSION_NUMBER < 0x02000200
# error "This version of Libevent is not supported; get 2.0.2-alpha or later."
#endif

#ifndef HPHP_DISABLE_EVENT_OPENSSL
# include <openssl/evp.h>
# include <openssl/x509.h>
# include <openssl/x509v3.h>
# include <openssl/crypto.h>
# include <openssl/pem.h>
# include <openssl/err.h>
# include <openssl/conf.h>
# include <openssl/rand.h>
# include <openssl/ssl.h>
# include <openssl/pkcs12.h>
#endif


#define HPHP_EVENT_TIMEVAL_TO_DOUBLE(tv) (tv.tv_sec + tv.tv_usec * 1e-6)
#define HPHP_EVENT_TIMEVAL_SET(tv, t)            \
  do {                                           \
    tv.tv_sec  = (long) t;                       \
    tv.tv_usec = (long) ((t - tv.tv_sec) * 1e6); \
  } while (0)

#define HPHP_EVENT_X_GET(__x, dest, src, def)         \
  auto dest = __x::get((src));                        \
  if (!dest) {                                        \
    raise_warning(#__x " object is not constructed"); \
    return def;                                       \
  }

#define HPHP_EVENT_CONFIG_GET(dest,     src, def) HPHP_EVENT_X_GET(EventConfig,      dest, src, def)
#define HPHP_EVENT_BASE_GET(dest,       src, def) HPHP_EVENT_X_GET(EventBase,        dest, src, def)
#define HPHP_EVENT_GET(dest,            src, def) HPHP_EVENT_X_GET(Event,            dest, src, def)
#define HPHP_EVENT_BUFFER_GET(dest,     src, def) HPHP_EVENT_X_GET(EventBuffer,      dest, src, def)
#define HPHP_EVENT_BEVENT_GET(dest,     src, def) HPHP_EVENT_X_GET(EventBufferEvent, dest, src, def)
#define HPHP_EVENT_DNSBASE_GET(dest,    src, def) HPHP_EVENT_X_GET(EventDnsBase,     dest, src, def)
#define HPHP_EVENT_SSLCONTEXT_GET(dest, src, def) HPHP_EVENT_X_GET(EventSslContext,  dest, src, def)

#define HPHP_EVENT_CHECK_CALLABLE(cb, ret)                                  \
do {                                                                        \
  if (!f_is_callable((cb))) {                                               \
    raise_warning("function '%s' is not callable", (cb).toString().data()); \
    return ret;                                                             \
  }                                                                         \
} while (0)


namespace HPHP {

/*
 * XXX Consider using SmartResource<Event> etc.
 */

extern const StaticString s__rc;
extern const StaticString s_EventConfig;
extern const StaticString s_EventBase;
extern const StaticString s_Event;
extern const StaticString s_EventBuffer;
extern const StaticString s_EventBufferEvent;
extern const StaticString s_EventUtil;
extern const StaticString s_EventDnsBase;
extern const StaticString s_EventSslContext;

// Base class for all C++ classes which represent corresponding PHP classes
class EventResourceData: public SweepableResourceData {
  public:
    template<class T>
    static T* getInternalResource(Object obj, const String& ctx) {
      if (obj.isNull()) {
        raise_error("NULL object passed");
        return nullptr;
      }

      //auto ret = Native::data<T>(obj.get());
      auto res = obj->o_get(s__rc, false, ctx);
      if (!res.isResource()) {
        return nullptr;
      }

      auto ret = res.toResource().getTyped<T>(false, false);
      if (!ret) {
        return nullptr;
      }

      if (!ret->isValid()) {
        raise_warning("Found unconstructed %s", ctx.c_str());
        return nullptr;
      }

      return ret;
    }

    virtual ~EventResourceData() {}
    virtual bool isValid() const = 0;

    Object newInstance(const String& ctx);
};


class EventExtension : public Extension {
  public:
    EventExtension() : Extension("event", "1.0.0-dev") {}
    virtual void moduleInit() {
      _initEventConfigClass();
      _initEventBaseClass();
      _initEventBufferClass();
      _initEventBufferEventClass();
      _initEventClass();
      _initEventUtilClass();
#ifndef HPHP_DISABLE_EVENT_EXTRA
      _initEventDnsClass();
#endif
#ifndef HPHP_DISABLE_EVENT_OPENSSL
      _initEventSslContextClass();
#endif
      loadSystemlib();
    }

  private:
    void _initEventConfigClass();
    void _initEventBaseClass();
    void _initEventBufferClass();
    void _initEventBufferEventClass();
    void _initEventClass();
    void _initEventUtilClass();
#ifndef HPHP_DISABLE_EVENT_EXTRA
    void _initEventDnsClass();
#endif
#ifndef HPHP_DISABLE_EVENT_OPENSSL
    void _initEventSslContextClass();
#endif
};


} // namespace HPHP

#endif // incl_HPHP_EXT_EVENT_H

// vim: et ts=2 sts=2 sw=2
